---
title: "Thank you for completing our survey!"
author: "Annika Tensi"
date: '2021-10-20'

url: /surveyMailingList/

---

## Sign up for our Newsletter

If you would like to become a OSC-W member and sign up for our Newsletter, please fill in <a href="https://forms.office.com/Pages/ResponsePage.aspx?id=5TfRJx92wU2viNJkMKuxjy3I_6n8HVlNkFcg-M2dtU5UNzdTQ0VVSUYxS0FERFBXWlhVQlRGU1ZCQSQlQCN0PWcu" target="_blank">this form </a> and receive information on news and upcoming events.  


## Contact us

You can also reach out to us via osc-wageningen@wur.nl

Also check out our [Twitter](https://twitter.com/OSCWageningen) for the most recent updates.

In any case, membership is noncommittal. We’re here to connect scholars interested in open science, but there are ways to become more involved if you’re interested!
