---
title: "Interactive lunch seminar: code standardisation and code peer review"
author: "A. Coiciu"
date: "2023-11-23T12:00:00+02:00"
signup_link: https://forms.office.com/Pages/ResponsePage.aspx?id=5TfRJx92wU2viNJkMKuxjy3I_6n8HVlNkFcg-M2dtU5UODJQTVMzWVdTTktZVFozVDIzN1JONEtXNCQlQCN0PWcu
summary: "Do you sometimes struggle with reproducing someone else’s code or understanding the code you wrote yourself a few months ago? You are not alone.
Modern-day scientists increasingly rely on code to wrangle, visualise, and analyse data, but these codes are often not free of hurdles and not readily applicable to your own data."
---

**{{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at 12.00 - 13.00 hrs**
<br>
*location: NIOO-KNAW*

#### <i>[Please sign-up here](https://forms.office.com/Pages/ResponsePage.aspx?id=5TfRJx92wU2viNJkMKuxjy3I_6n8HVlNkFcg-M2dtU5UODJQTVMzWVdTTktZVFozVDIzN1JONEtXNCQlQCN0PWcu)
Please note that that maximum number of participants in this workshop is 20

Do you sometimes struggle with reproducing someone else’s code or understanding the code you wrote yourself a few months ago? You are not alone.
Modern-day scientists increasingly rely on code to wrangle, visualise, and analyse data, but these codes are often not free of hurdles and not readily applicable to your own data.
 
In our new project, [CoreBirds](hhttps://www.researchequals.com/modules/m95v-7drc), we will create a library of documented and peer-reviewed codes. This library builds upon [SPI-Birds](https://spibirds.org/en), a grassroots initiative connecting those working on populations of individually-marked breeding birds, in which we build standardisation workflows to make data collected from these bird populations FAIR.
 
Interested in learning more about code standardisation and code peer review? Join our interactive OSC-W lunch seminar
Bring your lunch and laptop!

## About the presenters
<p>
<B>Stefan Vriend</B>
<p>
  <img width="250" src="/images/lunchEvents/picture1.png" alt="Stefan Vriend" />
</p>
<i>Passions:</i> pour-over coffee, socks, the colour brown
Stefan is data manager and developer at SPI-Birds and Coordinator Veluwe for LTER-LIFE, with a background in spatial ecology and population ecology.
</p>
<p>
<b>Joey Burant</b>
<p>
  <img width="250" src="/images/lunchEvents/picture2.png" alt="Joey Burant" />
</p>
<i>Passions:</i> cats, bird watching, the colour blue 
Joey is population ecologist and data enthusiast focussed on linking individual traits to population dynamics in the Department of Animal Ecology at NIOO-KNAW.

