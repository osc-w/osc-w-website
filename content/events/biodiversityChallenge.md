---
title: "An expedition to the WUR microcosmos"
author: "Ignacio Saldivia"
date: "2023-06-14"
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at **12.00 - 13.00 hrs**

<style>
  .wrapper {
    display: flex;
    flex-direction: row;
    justify-content: center; /*Center the images horizontally*/
    align-items: center; /*Center the images vertically*/
    margin-bottom: 20px; /*Add some space between the images and the text*/
  }

  .wrapper > div {
    margin-right: 15px;
  }

  .wrapper img {
    max-width: 100%;
    height: auto;
  }

  .text {
    margin-top: 20px;
  }
</style>

#### Hunt down microbes in the microcosmos, and learn about Open Hardware microscopes. This expedition is part of the Wageningen Diversity Challenge

<div class="wrapper">
  <div class="img1">
    <img src="/images/lunchEvents/biodiversityChallenge/thumbnail_image001.jpg">
  </div>
  <div class="img2">
    <img src="/images/lunchEvents/biodiversityChallenge/thumbnail_image002.jpg">
  </div>
</div>

<div class="text">

### Time & Location

  12.00 – 13.00 at the entrance of <a href="https://www.openstreetmap.org/way/297436705#map=17/51.98557/5.66077">Axis X</a>.

### By whom

  <a href="https://www.wur.nl/en/research-results/chair-groups/agrotechnology-and-food-sciences/biomolecular-sciences/bionanotechnology-2.htm">BioNanoTechnology</a>, in collaboration with the Open Science Community Wageningen

### Programme

  During the initial 20 minutes of this excursion, we will gather water and soil samples from various locations across the campus.

  Subsequently, we will reconvene in a meeting room, where we shall examine the collected samples, providing a glimpse into the hidden wonders of the microcosmos. To enhance our exploration, we will showcase state-of-the-art Open Hardware microscopes, including the Matchboxscope and the OpenFlexure.

  In the event of rain, the samples will be gathered beforehand, and we will exclusively engage in the microscope session

### Registration

  If you are eager to join us on this extraordinary quest through the microcosmos, kindly send an email to <a href="mailto:Vittorio.saggiomo@wur.nl">Vittorio.saggiomo@wur.nl</a>.

<br>
</div>

<div class="wrapper">
  <div class="img1">
    <img src="/images/lunchEvents/biodiversityChallenge/thumbnail_image003.jpg" />
  </div>
  <div class="img2">
    <img src="/images/lunchEvents/biodiversityChallenge/thumbnail_image004.jpg" />
  </div>
</div>
