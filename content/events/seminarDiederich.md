---
title: "Breakfast Seminar by Benedict Diederich"
author: "Ignacio Saldivia Gonzatti"
date: "2023-02-02"
---
<style>

.underlink a:link {
  color: #005172;
  text-decoration: underline;
}

.centered-image {
    display: block;
    margin: 0 auto; /*Center horizontally*/
    max-width: 100%;
    height: auto;
}
</style>

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at **09.30 - 10.30 hrs**

*Impulse, Speaker's Corner*

# Please sign up <a href="https://forms.office.com/Pages/ResponsePage.aspx?id=5TfRJx92wU2viNJkMKuxjy3I_6n8HVlNkFcg-M2dtU5URFJXSlVaTVJTM1RBU0g0SjhVQjVMQkxZSSQlQCN0PWcu" target="_blank">here</a>

Feel free to bring your own breakfast and discuss with fellow WUR researchers!

<h2>From Ideas to Globally Accessible Instruments</h2>

<img class="centered-image" src="/images/lunchEvents/diederich/Diederich_pic1.jpg" alt="Image 1">

<p>Pandemics, extinction of species, and antibiotic crises: These are not dystopian scenarios of the distant future, but current, observable problems that people globally are now trying to deal with. Science is responsible here for guiding the path, researching connections on a global level and disseminating its findings. However, technical expertise and financial investments in laboratory equipment and experiments are often limiting factors. What follows from this is the lack of accessibility to the scientific tools necessary for researching socially relevant topics.</p>

<img class="centered-image" src="/images/lunchEvents/diederich/Diederich_pic2.jpg" alt="Image 2">

<p>"Frugal Science" thinks of scientific instruments from a whole new perspective. Limited by the available budget and parts, one has to get creative to solve a scientific question posed by society. Following this idea, we have applied this method to the - in many situations - costly field of microscopy to provide a deeper insight into the microcosm for everyone, everywhere. With our modular, open-source optics toolbox UC2 (You.See.Too.), we provide teaching materials in STEM subjects and put powerful optical tools in the hands of those who need them most: Biologists, Chemists, Physicists, Computational scientists, and students. We prove its capabilities as a rapid prototyping tool for optics by rapidly developing a new method to capture large amounts of plankton in 3D using a light sheet and a holographic setup. The "HoLiSheet" costs less than the laptop that processes the data and is fully open-source.</p>

<h2>About the presenter</h2>

<p>
After doing an apprenticeship as an electrician, Benedict Diederich started studying electrical engineering at the University for Applied Science Cologne. A specialization in optics and an internship at Nikon Microscopy Japan pointed him to the interdisciplinary field of microscopy. After working for Zeiss, he started his PhD in the Heintzmann Lab at the Leibniz IPHT Jena. He focuses on bringing cutting-edge research to everybody by relying on tailored image processing and low-cost optical setups. Part of his PhD program took place at the Photonics Center at Boston University in the Tian Lab. A recent contribution was the open-source optical toolbox UC2 (You-See-Too) which tries to democratize science by making cutting-edge affordable and available to everyone, everywhere. He is co-founder of the open-hardware startup "openUC2" which aims to scale up open microscopy to solve global problems and spend some time during his Post Doc in Manu Prakash's lab at Stanford University to promote "Frugal Optics" in marine biology communities.
</p>

<img class="centered-image" src="/images/lunchEvents/diederich/Diederich_pic3.jpg" alt="Image 3">
