---
title: "Events"
author: "Annika Tensi"
date: '2022-03-15'
output:
  html_document:
    df_print: paged
tags:
- Open Science
- Wageningen
- Events
categories: null
---

# OSC-W Events all

We are organising regular events at WUR and in collaboration with other institutes, OSCs and initiatives. Here you find an overview of upcoming and past events.

Please click on the events below for further information on the date and time, location and for signing-up. 
