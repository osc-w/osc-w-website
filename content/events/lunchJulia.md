---
title: "Lunch Seminar by Julia Höhler"
author: "Ignacio Saldivia"
date: "2022-12-14"
---

# CANCELLED

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}, 12.30 - 13.30

*Leeuwenborch (room: B0075)*

Feel free to bring your own lunch and discuss with fellow WUR researchers!

## Importance of replication studies

A new replication study finds that farmers across eleven European farming systems avoid risks. Farmers also perceive losses stronger than gains and distort small probabilities. The study contributes to the credibility revolution in economics by showing that risk aversion, loss aversion, and probability weighting can be assumed for most farming systems.

Farming is a business full of uncertainty. Risk is not only about whether it will rain enough or whether a pest will ruin crops. To understand the agricultural sector, economists want to set up models which account for these risks, and they also want these models to include a good understanding of how much farmers would like to take or avoid risks. A first step for this is to know how risk loving or risk averse farmers are.

A team of 28 researchers from a network on the promotion of economic experiments for policy-making [reecap.org](https://sites.) has recently studied 1,430 farmers’ risk attitudes from eleven European countries.

### Method

We replicate a well-known method that uses risky gambles, in the form of lotteries, to learn about farmers’ risk preferences. This approach provides a general understanding of farmers’ risk attitudes that can serve as an input for policy simulation models.

### Results

The contribution of this research relates to replicability of science. In many cases, findings of one study cannot be extrapolated to other settings. Our work replicated a 2014 study undertaken by a team of French researchers in 2014. Our results show a similar pattern as the original application of lotteries to farmers.

When results are replicable, the possibility of these being correct increases and if replication is widespread, hypotheses can turn into theory. Therefore, the statement that farmers are risk and loss averse and give higher subjective probabilities to improbable events has now a stronger empirical basis.

From our findings we can conclude that for all our samples representing the different farming systems in specific countries:

1. Farmers are averse to risks. The most risk averse farmers are the Spanish and Italian olive growers, and the French potato and organic farmers.
2. Farmers are loss averse for all types of  farmers, but it is substantially larger for the Spanish olive growers than for the Dutch farmers.
3. Farmers give higher subjective probabilities to objectively improbable events across all farming systems.

### More information

The study was published in English in the journal Applied Economic Perspectives and Policy and is freely available under [this link](https://onlinelibrary.wiley.com/doi/10.1002/aepp.13330)

The original study was published in English in 2014 and is available under [this link](https://academic.oup.com/erae/article-abstract/41/1/135/530119?redirectedFrom=fulltext)

## About the presenter

Julia Höhler is an assistant professor at the Business Economics Group. Her research is focused on behavioural economics, cooperatives and sustainability in the food supply chain.
