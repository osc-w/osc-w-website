---
title: "Open Source software workshop"
author: "Ignacio Saldivia"
date: "2023-11-27T12:00:00+02:00"
signup_link: https://forms.office.com/e/ZPUCRCH8kf
summary: "Software as a Lab and Financing and partnership opportunities. We all recognize the advantages of open-source software in science and engineering. However, it has proven to be difficult to allocate time or to finance new staff for this endeavour."
---
**{{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at 12.00 - 13.30 hrs**
<br>
*Location: Speaker's Corner in Impulse*

#### [Please sign-up here](https://forms.office.com/e/ZPUCRCH8kf)

<p>We all recognize the advantages of open-source software in science and engineering. However, it has proven to be difficult to allocate time or to finance new staff for this endeavour.
In this seminar, we would like to cover two topics:
 
1. Software as a lab.
We aim to create a pilot course where we teach students not only how to code, but also how to properly document their software in a scientific way, which allows for reproducibility and verifiability of the research, following FAIR principles (findable, accessible, interoperable, and reusable).
How do you envision such a course:
<ul>
<li>Can it eventually become a mandatory course for BSc/MSc students who work on coding in their thesis? Or perhaps just an optional course for those interested in the topic?</li>
<li>Can we aim for a course that provides PhD students with an alternative communication output, instead of publishing their work only in journals, replacing one of their papers?</li>
<li>What would be the contents of the course?</li>
</ul>
 
2. Financing and partnership opportunities.
Are you interested in collaborating in the development of a platform where researchers can generate, share and collaborate on software development
<ul>
<li>What does your group need from such a platform? What can your group bring to the table?</li>
<li>What opportunities for funding do you know?</li>
</ul>
I will share my history of rejected proposals, to try and learn from past weaknesses.</p>
 
<b>About the presenter</b>
<p></p><p>
Daniel Reyes-Lastiri is a lecturer in the Agricultural Biosystems Engineering group, in the field of modelling and control. He did his PhD at WUR on modelling aquaponic systems. His recent work on tools for modelling uncertainty can be found in Sourceforge (documentation pending, as he is in the early stages of learning how to use Sphinx or similar packages). He is currently working on ideas to develop a platform for education and collaboration on scientific software, ranging from mathematical models to machine learning algorithms.
Feel free to bring your own lunch and listen to- and discuss with fellow Citizen Science and Open Science enthusiasts.</p>
