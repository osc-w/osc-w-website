---
title: "Lunch Seminar by Rosan van Halsema"
author: "Ignacio Saldivia"
date: "2023-03-09"
---
## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at **12.00 - 13.00 hrs**

*Location: [NIOO](https://goo.gl/maps/PS7Hv4fLgYnCU9ok8), Colloquium Room*

Directions: When you enter the building through the main entrance, inform the receptionist (on the right) that you've come for the OSC-W seminar. They will explain to you how to get to the colloquium room.

# Please sign-up [here](https://forms.office.com/e/aHJ97E2fVN)

Feel free to bring your own lunch and discuss with fellow researchers from WUR and NIOO-KNAW!

## Citizen Science and Water Quality in the Netherlands

‘Vang de Watermonters’ is a citizen science project that examines the water quality in small waters in the Netherlands. These waters are barely monitored for the Water Framework Directive. With the effort of citizens we can fill this blind spot of the water quality in the many ditches, ponds and streams that are in the Netherlands. The results of previous years show that the quality of the majority of small waters in the Netherlands is still insufficient. For good water quality clear water is needed with sufficient submerged water plants, which are an important source of oxygen and habitat under water.  

Citizens were trained via a webinar or live training to do the water quality measurements. Last summer 425 citizens measured the water quality on one or more locations, which resulted in 1680 measurements. A validation was performed on 135 locations to assess which measurements could be included in the water quality assessment. The results of the measurements were scaled up to all small water in the Netherlands based on water type and land use around the location of the measurement.

<figure style: align="middle">
  <img src="/images/lunchEvents/lunchRosan.jpg" width="400" height="400" alt="Collecting samples for the project ‘Vang de Watermonters’" />
  <figcaption align="center">Collection of samples for the project</figcaption>
</figure>

The measurements that were performed by the citizens include a mark for the location, a GPS location, and photos. In addition, the clarity of the water is measured, the coverage of aquatic plant groups are recorded, macro-invertebrate groups are identified and a water sample is taken for analysis of the nutrients in the water. These measurements are used to assess the ecological status of the water. Litter around the location is recorded for the purpose of awareness.  

More information about the project can be found (in Dutch) on <www.vangdewatermonsters.nl>

## About the presenter

Rosan van Halsema is a PhD student at the Netherlands Institute for Ecology (NIOO-KNAW) and working on a citizen science project called ‘Vang de Watermonsters’ that examines the ecological water quality in small waters in the Netherlands.
