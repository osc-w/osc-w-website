---
title: "Lunch Seminar by Daniel Reyes Lastiri + Photo Gallery"
author: "Ignacio Saldivia"
date: "2023-01-11"
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at **12.30 - 13.30 hrs**

*[Impulse](https://goo.gl/maps/gGAEbQyC2gcQ931L8), Speakers' Corner*

# Please sign-up [here](https://forms.office.com/r/fKEFcwreKG)

Feel free to bring your own lunch and discuss with fellow WUR researchers!

## Open Source for community model development in WUR and beyond

Mathematical models help to study, understand and improve agricultural systems. Students and researchers use these models in a diversity of fields, such as plant sciences, ecology, and biosystems engineering. Therefore, these modelling resources should be open to interdisciplinary collaboration.

However, combining this wide variety of scientific fields into single models often results in large complexity, which limits their applicability. Similarly, mathematical models are currently implemented in a wide variety of computer languages, spread across private and public repositories.

As a result, students and researchers often find themselves building models from scratch. Furthermore, every new model is often developed by PhD students in the early stages of their learning trajectory on modelling and programming, and the use of each new model often remains limited to the experts in the field.

We need to develop a standardised open framework for existing models in agriculture. Such framework can be hosted in repositories with Git version control, allowing for contributions by the community. But we also need support to finance the development and maintenance of such a framework.

If we can get it started, Wageningen UR has the potential to pioneer the development of an open framework for agricultural models, using the wide range of existing models within the university, and putting them to the test across different disciplines, allowing for potential contributions by hundreds of students every year.

## About the presenter

Daniel is a lecturer in the Farm Technology group in the field of modelling and control. He did his PhD at WUR on modelling aquaponic systems. His recent work on tools for modelling uncertainty can be found in [Sourceforge](https://sourceforge.net/p/vorsetmembership/code/ci/master/tree/) (documentation pending, as he is in the early stages of learning how to use Sphinx or similar packages).

## Photo Gallery

{{< image-gallery >}}
