---
title: "Lunch Seminar Citizen Science in the global south + Slides"
author: "Ignacio Saldivia"
date: "2023-06-29"
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}, 12.30 - 13.30

*Location: [Impulse](https://www.openstreetmap.org/way/297433831), Speakers' corner*

# Please sign-up [here](https://forms.microsoft.com/e/LpRxLQU9NX)

Feel free to bring your own lunch and listen to- and discuss with fellow Citizen Science and Open Science enthusiasts.

## Citizen Science in the global south

At WUR, Citizen Science is implemented in many different types of research areas around the world, leading to variations in motivation, implementation, and research techniques. In this seminar, David Walker, Samuel Sutanto, and Lisa Best will provide insights into their Citizen Science research conducted in the global south. Each of them will describe a case study of their project.

Building on the case studies, we will describe the often significant differences between citizen science projects in Europe/North America and projects in low/middle income countries. Common differences include: participant demographics and motivations, recruitment methods, time and effort of involvement, and the overall aims of projects. We will discuss how greater consideration of the experience of Citizen Scientists is needed to ensure both researchers and communities are benefitting.

## About the case studies

### *David Walker*

Citizen science projects in Ethiopia, South Africa and India involved regular rainfall, river/spring flow and groundwater level monitoring in areas of data scarcity. The data were used in models and combined with remote sensing in order to assess water resources availability and the performance of land degradation counter-measures. Another project in Brazil showcases the regular collection of qualitative data about how people experience drought in order to improve drought monitoring, early-warning and emergency response.

### *Samuel Sutanto*

The WATERAPPS programme utilises mobile technology to combine local knowledge with modelled weather forecasts. Farmers in Northern Ghana submit their local weather forecasts based on observed local ecological indicators and rainfall data measured from rain gauges. This co-production of climate information services enables adaptive decision-making.

### *Lisa Best*

Participatory 3-Dimensional Modelling was applied in an afro-descendent tribal community in Suriname to support territorial management through inclusive participation and social learning, and served as an empowering tool for further talks with policy makers and a range of actors with competing land-use interests.

## Presentation

<p align="center"><a href="/files/citizenScienceGlobalSouth.pdf" download="Citizen Science in the global">
<button  style="background:#4CAF50;color:white;border: none;"
class="btn"> Download in PDF <i class="fa fa-download"></i></button></a> <p>

<center>
  <body>
    <iframe
    src="/files/citizenScienceGlobalSouth.pdf#toolbar=0" width="100%" height="500px">
    </iframe>
  </body>
</html>
</center>
