---
title: "Lunch Seminar by Yizhou Ma"
author: "Annika Tensi"
date: "2022-09-29"
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}, 12.30 - 13.30

*Orion (room: B4044)*

# Please sign up [here](https://forms.office.com/r/TqLGj6Hwsk)

During this lunch seminar, Yizhou Ma will present his open-source software development project on 3D food printing. Further information can be found [here](/award/yizhou_open-source/).

## Open-source tool building for food manufacturing challenges

Producing safe, healthy, sustainable, and delicious foods is often a mix of arts and sciences. Sometimes, an extra pitch of hops added by the master brewer can vitalize a boring pale ale to a refreshing and flavorful IPA. Despite the great result, the master brewer may have little explanation to his or her action, other than claiming ‘I felt like adding a bit more this time’. In fact, foods are diverse and subject to geographical, seasonal, and genetic variabilities, so food processing methodologies are constantly being modified, readjusted, and/or renovated based on the specific production needs. This process relies on experience-based expertise, and it is hardly transferrable through digital means. In this seminar, we will share a few examples of open-source software development for food manufacturing. The developed tools unify learnings from operators and digitize them for scalable uses. They are built as open-source and target to provide general solutions to a set of similar problems. We will discuss a typical process of developing such tools in food manufacturing settings, challenges of publicizing the open-science mindset to the food industry, and brainstorm ways to move forward to building a community around such efforts.  

## About the presenter

Yizhou Ma is a PhD candidate from the Food Process Engineering chair group at Wageningen University. His PhD thesis focuses on the adaptive control of extrusion-based 3D food printing. He develops sensor applications to monitor the food printing process and models the sensor data to make control decisions that improve food printing efficiencies. Prior to his PhD, Yizhou worked as a senior R&D scientist at Gastrograph AI, where he developed predictive sensory analytics using natural language processing and machine learning models. His professional pursuits are built upon global responsibility, data-driven research, and sustainable system design.
