---
title: "Recognition and rewards"
author: "Ignacio Saldivia"
date: "2023-09-14T13:15:00+02:00"
signup_link: https://forms.office.com/pages/responsepage.aspx?id=5TfRJx92wU2viNJkMKuxj-WIH-2jR4JNsPb22u0XxvBUQVY0OVdDRDc2QVRURzhMRFo5WjQxM1dRTS4u
summary: "Theo Jetten will present the new proposed academic career framework at WUR, which is foreseen to take effect from 2024 onwards. The framework is a new system of recognition and rewards for all academic staff, i.e. lecturers, assistant, associate and personal professors and researchers."
---
## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}, 13.15-14.00

*Location: Orion building (hybrid), room B4045*

# Please register [here](https://forms.office.com/pages/responsepage.aspx?id=5TfRJx92wU2viNJkMKuxj-WIH-2jR4JNsPb22u0XxvBUQVY0OVdDRDc2QVRURzhMRFo5WjQxM1dRTS4u)

Feel free to bring your own lunch and listen to- and discuss with fellow Citizen Science and Open Science enthusiasts.

## Recognition and rewards: What is in it for you regarding Open Science?

Theo Jetten will present the new proposed academic career framework at WUR, which is foreseen to take effect from 2024 onwards. The framework is a new system of recognition and rewards for all academic staff, i.e. lecturers, assistant, associate and personal professors and researchers. The recognition and rewards of research outputs, such as research data and software, will also impact support staff (e.g. model stewards or data stewards). Theo will take you through the new proposed way of recognizing and rewarding activities, with specific attention to Open Science and Research Data.

*This meeting is organised by the Open Science Community Wageningen and the WUR Data Steward Network. Theo Jetten is one of the members of the WUR committee Recognition and Rewards and also the project leader. The meeting is semi-hybrid, you can ask your question via the chat but not directly communicate.*
