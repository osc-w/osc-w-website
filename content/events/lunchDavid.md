---
title: "Lunch Seminar by David Katzin + Slides"
author: "Annika Tensi"
date: "2022-10-27"
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}, 12.30 - 13.30

*Orion (room: B8020)*

# Please sign-up [here](https://forms.office.com/r/pMXyAzx4ba)

## Breaking the closed source code barrier

Open source coding is the practice of making the source code of programs publicly available for reading and modifying. In scientific modelling, habits and attitudes towards open source coding vary between disciplines and across institutes. In this seminar and open discussion, David Katzin will share his experience with open source modelling in the domain of greenhouse horticulture. Greenhouse models have been developed and published since as early as 1983, but the first open source model appeared in 2017. Using this example, we will discuss the factors that facilitate and impede open source coding: what are potential benefits and drawbacks? What are the attitudes toward this practice? How does open source fit within the general framework of open science? And what steps should we take to make our own code open source?

## About the presenter

Dr David Katzin is a researcher at the Wageningen Research Greenhouse Horticulture and Flower Bulbs unit. In 2020 he published *GreenLight*, the first open-source, validated, and peer-reviewed greenhouse climate and crop model. The model and some of his other projects are available at [github.com/davkat1](github.com/davkat1).

## Presentation

<p align="center"><a href="/files/20221027_OSC-W_OpenSource.pdf" download="DrDavidKatzin_OSCW_Presentation">
<button  style="background:#4CAF50;color:white;border: none;"
class="btn"> Download in PDF <i class="fa fa-download"></i></button></a> <p>

<center>
  <body>
    <iframe
    src="/files/20221027_OSC-W_OpenSource.pdf#toolbar=0" width="100%" height="500px">
    </iframe>
  </body>
</html>
</center>
