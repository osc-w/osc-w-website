---
title: "Launch Event 2022 + Photo Gallery"
author: "Annika Tensi"
date: '2022-05-13'
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at  **14.00 - 17.00 hrs** & open bar

**Omnia Building WUR Campus (room: Momentum)**

The Open Science Community Wageningen invites researchers, students, supporting staff and anyone that is interested to the official launch event. We would like to welcome colleagues from WUR and NIOO-KNAW.

The registration deadline passed. The number of places are limited. If you want to take part in the event, pleae send us an e-mail (<osc-wageningen@wur.nl>) and we can check whether a last-minute participation is possible.

## Programme

Friday, 13 May
|             | |             |
| ----------- |-| ----------- |
|14.00 | | **Welcome and registration** |
|             | |             |
|14.10 | | **Opening** |
|             | |             |
|14.30 | | **Why is science becoming non-repeatable?** |
|             | | Vittorio Saggiomo <p style = "font-size:11pt; font-style:italic"> Assistant Professor, BioNanoTechnology, WUR</p> <p style = "font-size:11pt"> Although everyone by now is familiar with the irreproducibility of science, another, probably more important problem is arising: the non-repeatability of science. Why, with all the technology at our disposal, the description of materials and methods is worse than hundreds of years ago? Why doesn’t a researcher want other researchers to repeat, use, or modify their research? In this short lecture, we will see the non-repeatability problem using a few examples, and try to understand where is the problem and how to solve it using Open Science. </p>|
|             | |             |
|14.55 | | **3R in Life Science: Repeatability, Replicability and Reproducibility** |
|             | | Edoardo Saccenti <p style = "font-size:11pt; font-style:italic"> Assistant Professor, Laboratory of Systems and Synthetic Biology, WUR</p> <p style = "font-size:11pt"> Repeatability, Replicability and Reproducibility are the corner stones of Science and Scientific research: they inform the fundamental principle according to which the same results must be obtained when experiments and data analysis are performed  by others than the original researchers. Stated otherwise, using the same experimental protocols (but also different protocols) the same findings must be achieved within the limit of the experimental error. Unfortunately, this is not the case: a large majority of results in biology and related disciplines are not 3R. In this talk will discuss some of the reasons, with a focus on data analysis  pitfalls commonly encountered in practice. </p>|
|             | |             |
|15.20 | | **The Dark Side of Science: Misconduct in Biomedical Research** |
|             | | Elisabeth Bik <p style = "font-size:11pt; font-style:italic">Elisabeth Bik, PhD is a Dutch-American microbiologist who has worked for 15 years at Stanford University and 2 years in industry. Since 2019, she is a science integrity volunteer and occasional consultant who scans the biomedical literature for images or other data of concern and has reported over 5,000 scientific papers. For her work on science communication and exposing threats to research integrity she received the Peter Wildy Prize, the John Maddox Prize, and the Ockham Award.</p> <p style = "font-size:11pt"> Science builds upon science. Even after peer-review and publication, science papers could still contain images or other data of concern. If not addressed post-publication, papers containing incorrect or even falsified data could lead to wasted time and money spent by other researchers trying to reproduce those results. Elisabeth Bik is an image forensics detective who left her paid job in industry to search for and report biomedical articles that contain errors or data of concern. She has done a systematic scan of 20,000 papers in 40 journals and found that about 4% of these contained inappropriately duplicated images. In her talk she will present her work and show several types of inappropriately duplicated images and other examples of research misconduct. In addition, she will show how to report scientific papers of concern, and how journals and institutions handle such allegations. </p>|
|             | |             |
|16.20 | | **Lighthouse Award** |
|             | | <p style = "font-size:11pt"> With the Open Science Lighthouse Award we want to celebrate and reward WUR researchers and researchers from associated institutes who have used open practices and tools to make their research more transparent, accessible and reproducible. The award winner will presented her or his case We hope that the Open Science success stories of WUR researchers encourage more colleagues to open up their science and promote the bottom-up adoption of open science practices across the science groups and research institutes. [More information](https://openscience-wageningen.com/lighthouse/) </p>|
|             | |             |
|16.50 | | **Closing** |
|             | |             |
|17.00 | | **Open Bar** |
|             | |             |

## Photo Gallery

{{< image-gallery >}}
