---
title: "Lunch Seminar: Peer Community In + Slides"
author: "Annika Tensi"
date: "2022-11-23"
twitterImg: /static/images/logo.png
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}  at **12.30 - 13.30 hrs**

*Forum (room: B0417)*

# Please sign-up [here](https://forms.office.com/r/iZHz6RmuYH)

Feel free to bring your own lunch and discuss with fellow WUR researchers!

## Open Peer Review systems under review

The publication landscape is in continuous evolution, and its shifting trends affect how researchers disseminate their work and are evaluated for it. Currently, authors can share their studies by publishing preprints: academic manuscripts that have not been peer-reviewed or published in a traditional venue, but on public servers such as arXiv and bioRxiv. Preprints speed up the communication of academic findings, allowing researchers and non-scientists to rapidly access study results and build upon each other’s work.

Recently, platforms for open peer review of preprints have emerged, including [Peer Community In (PCI)](https://peercommunityin.org/), PeerRef, preLights, PREreview and Review Commons. Particularly, PCI is a leading non-profit organisation devoted to peer review, recommendation and publication of scientific works in multiple thematic areas, at no cost. In this event, we will discuss what PCI and the associated [Peer Community Journal](https://peercommunityjournal.org/) are, how they work, how they contribute to the publication system and how researchers can best make use of them.

## About the presenters

WUR researcher Daniel Tamarit (Laboratory of Microbiology) initiated and co-hosts this lunch seminar. Daniel invited Thomas Guillemaud and Denis Bourguet, senior INRAE scientists at the Institute of Sophia Biotech (Sophia Antipolis, France) and at the Center of Biology for Population Management (Montpellier, France) respectively. They have conducted researches in population ecology, genetics and evolution, and are two of the three founders of Peer Community In and the Peer Community Journal.

## Further information

If you want to know more about PCI, please consider visiting the following websites:

[The PCI website](https://peercommunityin.org)

[The PCI Manifesto](https://peercommunityin.org/pci-manifesto/)

[A blog post on how to create a new PCI](https://peercommunityin.org/2019/05/21/steps-in-the-creation-of-a-new-pci/)

[The Peer Community Journal website](https://peercommunityjournal.org/)

## Presentation

Below you can find the presentations from the speakers.

<p align="center"><a href="/files/PreprintPreprintEvaluationSystems.pdf" download="PreprintEvaluationSystems_OSCW_Presentation">
<button  style="background:#4CAF50;color:white;border: none;"
class="btn"> Download in PDF <i class="fa fa-download"></i></button></a> <p>

<center>
  <body>
    <iframe
    src="/files/PreprintPreprintEvaluationSystems.pdf#toolbar=0" width="100%" height="500px">
    </iframe>
  </body>
</html>
</center>

<p align="center"><a href="/files/PresentationPeerCommunityIn.pdf" download="PeerCommunityIn_OSCW_Presentation">
<button  style="background:#4CAF50;color:white;border: none;"
class="btn"> Download in PDF <i class="fa fa-download"></i></button></a> <p>

<center>
  <body>
    <iframe
    src="/files/PresentationPeerCommunityIn.pdf#toolbar=0" width="100%" height="500px">
    </iframe>
  </body>
</html>
</center>

## Photo from the event

<p align="center">
  <img src="/images/lunchEvents/PeerCommunityIn.jpeg" alt="Image 1" />
</p>
