---
title: "Roadshow 4TU"
author: "Ignacio Saldivia"
date: "2023-06-14"
---
## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}

## How to publish your research data

Learn about publishing your research data, including guided dataset upload!

### Time & Location

• 10:00-11:30 - ESG - Gaia building (Gaia 2)

• 12:30-14:00 - AFSG - Axis-X (Large room)

• 15:00-16:30 - ASG - Zodiac (A0107)

### What to bring

Your own dataset and laptop

### Programme

• Intro “Why you should publish your dataset”

• Intro to TDCC NES and opportunities to join the community

• 4TU.ResearchData community and opportunities

• DEMO of Djehuty

• Q&A with Data Stewards and guided dataset upload

### Registration

Going to join us? Then please register at the bottom of this [webpage](https://community.data.4tu.nl/2023/05/24/next-stop-of-the-4tu-researchdata-roadshow-wageningen-university-research/)

### About the presenters

The roadshow is organised by 4TU ResearchData, in collaboration with the Open Science Community Wageningen and the WUR Data Steward Network. Presenters are: [Laura Zeeman](https://www.vcard.wur.nl/Views/Profile/View.aspx?id=37414) (WUR Library), [Madeleine de Smaele](https://www.tudelft.nl/library/research-data-management/r/support/data-stewardship/contact/madeleine-de-smaele), and [Roel Janssen](https://community.data.4tu.nl/members/rrejanssen/) (4TU.ResearchData). After the demo, you can start practising yourselves.
