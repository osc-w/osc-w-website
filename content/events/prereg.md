---
title: "Preregistration Lunch Seminar by Harm Veling + Recording"
author: "Annika Tensi"
date: '2022-06-09'

---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}, 12.30 - 13.30

**Leeuwenborch (room: B0075)**

## Preregistration: Why, What, How?

During this lunch seminar an introduction will be given to one specific open science practice, which is called preregistration. Preregistration entails specifying and reporting the steps that are needed to answer a research question before conducting research and/or before analyzing data. One important purpose of preregistration is to make visible to yourself and others what are the confirmatory and exploratory findings of a research project. More generally, preregistration aids understanding of the reliability of research findings, which lies at the heart of advancing science. Structured formats are available to help researchers with implementing preregistration, but these formats may also raise some questions about what is exactly expected. This presentation is meant to help researchers with answering such questions. For instance, the presentation will touch upon questions such as whether you should preregister your complete theoretical rational, or how much detail to give in a preregistration of your statistical analyses. It will also offer some ideas on what to do when you make an error in a preregistration, or how to deviate from a preregistration. After this presentation, you should have a basic idea of why and when preregistration is a useful practice, have some ideas about what to preregister, and have some handles to deal with deviations and errors.  

## About the presenter

Harm Veling is a professor at Consumption and Healthy Lifestyles, and mainly examines the nature of automatic decisions for attractive products, and how to modify these decisions with psychological interventions. He applies preregistration in almost all of his research projects since 2015, and has experienced the great benefits of this practice. For instance, preregistration in his work has contributed to the development of an application that very reliably changes preferences for food without using reward or punishment.  

## Recording of the seminar

{{< youtube-enhanced id="3Hh0Bz_Ckys"  >}}
