---
title: "Lunch Seminar by Jasper Koehorst"
author: "Ignacio Saldivia"
date: "2023-02-14"
---

## {{< var.inline >}}{{ .Page.Params.date.Format "02 January 2006" }}{{< /var.inline >}}, 12.30 - 13.30

*Impulse, Speakers’ Corner*

# Please sign-up [here](https://forms.office.com/e/EbxJCuhDmf)

Feel free to bring your own lunch and discuss with fellow WUR researchers!

## FAIR Open Science in the world of microbial communities and beyond

In the current era FAIR and Open Science are an essential aspect of research and the question is how do you do it?

In this talk Dr. Jasper Koehorst will present his work on the FAIR By Design principles and how he applied this in his current research on microbial communities.

## About the presenter

Jasper Koehorst is a researcher at the Laboratory of Systems and Synthetic Biology and part of the [UNLOCK](https://m-unlock.nl/) infrastructure for microbial communities. Most of his work is focused on the development of FAIR computational methods and  on the unraveling the mysteries of microbial systems.
