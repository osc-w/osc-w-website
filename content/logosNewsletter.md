---
title: "OSC-W Logos"
author: "Annika Tensi"
date: '2023-08-30'

---

<img src = "/images/logos/LogoThumb_OSCWageningen_2022_white.png" alt="Logo Thumb"/>
<img src = "/images/logos/LogoLong_OSCWageningen_2022_white.png" alt="Logo long"/>
