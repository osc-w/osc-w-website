---
title: "About"
author: "Annika Tensi"
---


<html>
<head>
  <style>
 hr {
  border: 0;
  border-top: 2px solid grey;
 }
 .button {
        background-color: #045474;
        border: none;
        color: white;
        padding: ;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        margin: ;
        cursor: pointer;
    }
  </style>

</head>

<body>

<p style="text-align:center;"><img src="/images/logos/logoOSClong.png" width="400"  alt="Logo"></p>

<p style="clear:both; text-align: left; font-size: 18px;" >
Open Science Communities, like the OSC-W, are independent, bottom-up local communities made up of members of various scholarly disciplines and career stages.  
<p/>

<h2> Our Vision </h2>

<p style=" text-align: left; font-size: 18px;" >
<b>Open science will be the norm in our research.</b> Open science practices and values are embedded in all workflows <b>across all disciplines</b>. <br>  <br>  Our science will be <b>accessible</b>, <b>reproducible</b>, <b>transparent</b>, and <b>inclusive</b>. <p/>

<h2> Our Mission </h2>

<p style="clear:both; text-align: left; font-size: 18px;" >
We are a <b>bottom-up</b> community <b>introducing</b> and <b>fostering</b> <i>open science practices</i> at Wageningen University & Research (WUR) and associated research institutes that stimulates <b>transparency</b>, <b>accessibility</b>, and <b>reproducibility</b> of responsible research across all disciplines.  <br>  <br> Thereby, we encourage collaboration and make science more <b>efficient</b>, <b>inclusive</b>, and <b>influential</b>. </p>

<h2> Our Goal </h2>

<p style="clear:both; text-align: left; font-size: 18px;" >
The OSC-W is a platform for open science newcomers and experienced researchers, to inspire each other and embed open science practices and values in their workflows, provide feedback onpolicies, infrastructure, and support services.  </p>
  
<h2> Our Target Group </h2>

<p style="clear:both; text-align: left; font-size: 18px;" >
Researchers, staff, institutional stakeholders, and students who have never heard about, who are curious towards, and who are experienced with open science practices. </p>

<p style="text-align:center">
<a style="background-color: #34b434; font-size: 20px;" href="/about/getintouch" class="button">Get in touch</a> <br> <br>
<a style=" background-color: #045474;font-size: 20px;" href="/about/whoarewe" class="button">Meet the OSC-W Members</a>  </p>

</body>
</html>
