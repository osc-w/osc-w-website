---
title: "Who are we"
author: "Annika Tensi"
---

<html>
<head>
  <style>
 hr {
  border: 0;
  border-top: 2px solid grey;
 }
 .left-details {
  text-align: left;
  direction: ltr;
}
.right-details {
  text-align: right;
  direction: rtl;
}
.left-align {
  text-align: left;
  direction: ltr;
  clear: both;
}
.right-align {
  text-align: right;
  direction: rtl;
  clear: both;
}
.left-image {
  float: left;
  margin: 0px 15px 15px 0px;
}
.right-image {
  float: right;
  margin: 0px 0px 15px 15px;
}
  </style>
</head>

<body>

<p style="clear:both; font-size: 18px;" >  
<b>Currently, we are a group of researchers and staff from WUR. Our community comprises academics and support staff from different disciplines and career stages. All researchers, students, and support staff of WUR and NIOO-KNAW at all career stages are welcome!
</b>
<p/>

<p style="text-align:center;"><img style=" margin: 0px 0px 30px 0px;" alt="Picture of the OSC-W core group" src= "/./images/profilepicsmembers/2205coregroup.jpg" width="50%" height="100"></p>

<!-- add underline -->
<h4 style="margin-bottom: 50px;"> Get to know us and find out why we are enthusiastic about Open Science:
</h4>

<!-- Add members in alphabetical order -->
  <img class="left-image" alt="Profile Picture Annika" src= "/./images/profilepicsmembers/Annika.jpg" width="150" height="100">
  
  <p class="left-details"><b>Annika Tensi</b> <br> <i>PhD Candidate</i> <br> Business Economics, WUR <br> Chair between 2021 and 2023</p>
  
  <p class="left-align"> I believe that Open Science practices and tools have the power to fight fraudulent practices and frustrations through transparency and reproducibility. For instance, pre-registering hypotheses and analyses fights p-hacking and HARKing, which in turn leads to more trustworthy scientific results. Also, sharing understandable and reproducible data and code fights the file drawer problem and duplications.&lrm;

  Of course, being open makes us more vulnerable, too, since we potentially also share our mistakes. That means, also the research culture needs to change. It should be regarded as an achievement to make and learn from mistakes through scientific exchange. I believe that Open Science helps to change research cultures for the better.&lrm;

</p>
<hr/>
  <img class="right-image" alt="Profile Picture Mark" src= "/./images/profilepicsmembers/Mark.jpg" width="150" height="100">
  <p class="right-details"><b>Mark Sterken</b> <br> <i>Associate Professor</i> <br> Laboratory of Nematology, WUR </p>
  <p class="right-align"> In my research me and my team rely on data-heavy approaches in the field of genomics, transcriptomics and genetics. I have benefitted in the past from colleagues and groups making data, protocols and analytical pipelines openly available. However, I know that this is not yet the standard. For my teams research I aspire to make it as open as possible and in this quest I would like to take the broader open science community of Wageningen along.&lrm;
</p>
<hr/>
 <img class="left-image" alt="Profile Picture Ana" src= "/./images/profilepicsmembers/Ana.png" width="150" height="100">
  <p class="left-details"><b>Ana Coiciu</b> <br> <i>PhD Candidate</i> <br> Consumption & Healthy Lifestyles, WUR </p>
  <p class="left-align">As an upcoming scientist, integrity is one of my most valued principles. 
Open science provides the opportunity for us to take pride in both our achievements and mistakes, learning about the process by which we acquire our knowledge. I believe that open science is crucial to a culture of transparency and accelerated discovery in research, bridging the gap between different data resources, disciplines, and society.&lrm;
</p>
<hr/>
 <img class="right-image" alt="Profile Picture Chantal" src= "/./images/profilepicsmembers/Chantal.jpg" width="150" height="100">

  <p class="right-details"><b>Chantal Hukkelhoven</b> <br> <i>Specialist Open Science & Education, Open Access</i> <br> WUR Library </p>

<p class="right-align" > Open Science enables researchers to build upon each other’s work and innovate at a faster pace. When I was a researcher, my colleagues and I could share and exchange datasets, models and knowledge with others. This enabled us to study new research questions and investigate existing questions in larger and more diverse datasets - resulting in better-fitting models. Also making results accessible to non-scientists appeared to be valuable and increased the impact of our work. All these advantages are facilitated by the transition towards Open Science.&lrm;

</p>
  
<hr/>

  <img class="left-image" alt="Profile Picture Edoardo" src= "/./images/profilepicsmembers/Edo.png" width="150" height="100">
  
  <p class="left-details"><b>Edoardo Saccenti</b> <br> <i>Assistant Professor</i> <br> Systems and Synthetic Biology, WUR </p>
  
  <p class="left-align" > The scientific method changed the history of humanity, opening the way to all discoveries and inventions that shaped and are shaping the world. For many varied reasons science today is often under attack and the unprecedented and unforecasted reproducibility crisis is harming science and the work of many devoted scientists. For this reason, I believe that it is important that everybody embraces the principles of Open Science to aim to achieve the goal of a Science that is truly reproducible and inclusive.&lrm;

</p>

<hr/>

  <img class="right-image" alt="Profile Picture Fränzel" src= "/./images/profilepicsmembers/VanDuijnhoven.jpg" width="150" height="100">
  
  <p class="right-details"><b>Fränzel van Duijnhoven</b> <br> <i>Assistant Professor</i> <br> Human Nutrition & Health, WUR</p>

  <p class="right-align" > I strongly believe that science as well as society benefits most from working together, sharing our knowledge, and making our research and education openly available. However, making science and its practices completely ‘open’ is not an easy task for various reasons. I hope to accelerate this process by sharing experiences and learning from each other within the Open Science Community Wageningen.&lrm;

</p>

<hr/>

  <img class="left-image" alt="Profile Picture Harm" src= "/./images/profilepicsmembers/Harm.jpg" width="150" height="100">
  
  <p ><b>Harm Veling</b> <br> <i>Professor</i> <br> Consumption & Healthy Lifestyles, WUR <br> Core Team Member in 2022, Chair from 2023 onwards </p>
  
  <p class="left-align" > Harm Veling is a professor at Consumption and Healthy Lifestyles and examines the nature of automatic decisions for attractive products, and how to modify these decisions with psychological interventions. He applies preregistration and other open science practices in most of his research projects since 2015 and has experienced the great benefits of these practices. For instance, preregistration in his work has contributed to the development of an application that very reliably changes preferences for food without using external reinforcement. Based on his experience he aims to positively promote open science practices as he is convinced that this will not only benefit researchers who adopt these practices but also the scientific community and society.&lrm;

</p>

<hr/>

  <img class="right-image" alt="Profile Picture Jose" src= "/./images/profilepicsmembers/Jose.jpg" width="150" height="100">
  
  <p class="right-details"><b>Jose Lozano Torres</b> <br> <i>Assistant Professor</i> <br> Laboratory of Nematology, WUR </p>

  <p class="right-align" > I believe that Open Science can truly open the doors for a positive transformation in the world. I was first educated as an agricultural engineer in Colombia and coming to the Netherlands to further develop my career made me realize that information asymmetry in Science and Technology benefits only a selected group of people. I think we, as privileged scientists at WUR and for the benefit of all humanity, have a major responsibility to make our work more accessible, more transparent, more equal, and more collaborative.&lrm;

</p>

<hr/>

  <img class="left-image" alt="Profile Picture Julia" src= "/./images/profilepicsmembers/Julia.jpg" width="150" height="100">
  
  <p ><b>Julia Höhler</b> <br> <i>Assistant Professor</i> <br> Business Economics, WUR </p>

  <p class="left-align" > As a master's student, I asked a team of authors for their questionnaire and never received a response. This lack of transparency frustrated me and made my work more difficult. As researchers, we stand on the shoulders of giants. I believe that Open Science can help us reach even higher and further.&lrm;
</p>

<hr/>

  <img class="right-image" alt="Profile Picture Marijke" src= "/./images/profilepicsmembers/Marijke.jpg" width="150" height="100">
  
  <p class="right-details"><b>Marijke ten Brinke</b> <br> <i>Information specialist</i> <br> Library & Information Services, NIOO-KNAW </p>

<p style="clear:both; text-align: justify; direction: rtl;" >
Both in her work as an information specialist and as part of the OSC-W core team, Marijke strives to make information about Open Science comprehensible and easily accessible, in order to make it as easy as possible for researchers to bring Open Science into practice.&lrm;
</p>

<hr/>

  <img class="left-image" alt="Profile Picture Murilo" src= "/./images/profilepicsmembers/Murilo.jpg" width="150" height="100">
  
  <p class="left-details"><b>Murilo de Almeida Furtado</b> <br> <i>PhD Candidate</i> <br> Business Economics, WUR <br> Core Team member between 2021 and 2023<br>Core Team member between 2021 and 2023 </p>

  <p class="left-align" > Once I could understand how unequal opportunities could constrain society's development, I could quickly understand the importance of open, accessible and democratic education. Some people say that happiness is only real when shared, and I believe that the same applies to knowledge. Quoting Paulo Freire, an important educator from my home country, “Education does not change the world. Education changes people. People change the world”. If science can help build a better world, I have no doubt that one of the best ways it can do it is through Open Science.&lrm;

</p>

<hr/>

  <img class="right-image" alt="Profile Picture Vittorio" src= "/./images/profilepicsmembers/Vit.jpg" width="150" height="100">
  
  <p class="right-details"><b>Vittorio Saggiomo</b> <br> <i>Assistant Professor</i> <br> BioNanoTechnology, WUR </p>

  <p class="right-align" > Repeatability and reproducibility are the main foundation of science. However, although the computer and internet advancements, sharing knowledge, data, experimental set-ups, and so on is still at the same level, if not worse, than hundreds of years ago. Why are scientists not openly sharing methods for reproducing their research? Or raw data of their experiments? Why, with all the electronic tools we have at our disposal nowadays, is sharing scientific knowledge still lacking? I don’t have good answers to these questions, but we can change this attitude together using Open Science. I am mainly focussing on Open Hardware development.&lrm;

</p>

<hr/>

  <img class="left-image" alt="Profile Picture Yann" src= "/./images/profilepicsmembers/Yann.jpg" width="150" height="100">
  
  <p class="left-details"><b>Yann de Mey</b> <br> <i>Associate Professor</i> <br> Business Economics, WUR<br>Core Team member between 2021 and 2023</p>

  <p class="left-align"> I am passionate about helping support good open science practices as I believe researchers/educators should invest their time at the frontier of the field rather than being forced to re-invent the wheel several times. With this initiative, I hope we can inspire and support many colleagues to openly share their materials, data, manuscripts and many things beyond rather than making them “available upon reasonable request”.&lrm;

</p>

</body>
</html>
