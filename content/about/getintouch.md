---
title: "Get in touch"
author: "Annika Tensi"
---

<p style="clear:both; text-align: left; font-size: 18px;">
You can reach us at <a href="mailto:osc-wageningen@wur.nl"> osc-wageningen@wur.nl </a> <p/>

<h2> Becoming a member </h1>

<p style="clear:both; text-align: left; font-size: 18px;"> Becoming a member is simple: Just sign up to our <a href="https://forms.office.com/Pages/ResponsePage.aspx?id=5TfRJx92wU2viNJkMKuxjy3I_6n8HVlNkFcg-M2dtU5UNzdTQ0VVSUYxS0FERFBXWlhVQlRGU1ZCQSQlQCN0PWcu" target="_blank">Newsletter</a> where we share upcoming events and news.
<br> <br>
Also check out our <a href="https://twitter.com/OSCWageningen" target="_blank"> Twitter </a> for the most recent updates.
<br> <br>
In any case, membership is noncommittal. We’re here to connect scholars interested in open science, but there are ways to become more involved if you’re interested!
<p/>
