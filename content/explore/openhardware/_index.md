---
title: "Open Hardware Wageningen"
author: "Vittorio Saggiomo"
date: '2023-04-11'
---

<center><h1>Open Hardware Wageningen</h1></center>
<p align="center">
<img align="center" src="/images/logos/thumbnail_OHWageningen.png" alt="OH Logo">
</p>

## What is Open Hardware?

Open hardware refers to making hardware designs and specifications publicly available, allowing others to study, modify, distribute, and manufacture the hardware.

This movement is similar to open-source software but involves sharing physical designs and schematics instead of computer code. Finally, the open hardware movement seeks to create a collaborative and transparent environment for creating and distributing hardware.

The goal is to encourage innovation and collaboration among individuals and organizations by removing barriers to access and allowing for greater participation in technology development. Examples of open hardware projects include 3D printers, robotics kits, and electronic circuits.

## Why Open Hardware?

Open Hardware can improve science in many different ways:

1. **Reproducibility**: Open hardware provides a way to document and share the designs of scientific instruments and equipment. This makes it easier for other researchers to replicate the equipment and conduct similar experiments, improving scientific results' reproducibility.
2. **Collaboration**: Open hardware encourages collaboration among scientists and engineers, as they can contribute to the design and development of scientific equipment. This can lead to the developing of more advanced and efficient equipment and accelerate scientific progress.
3. **Cost-effectiveness**: Open hardware often uses low-cost and widely available materials and components, which makes scientific equipment more affordable and accessible. This can benefit scientists and researchers in low-resource settings, as well as those who work with limited budgets.
4. **Innovation**: Open hardware provides a platform for innovation and creativity, as researchers and engineers can modify and improve existing designs or create entirely new designs. This can lead to developing new scientific instruments and equipment, advancing scientific knowledge and discovery.
5. **Accessibility**: Open hardware can be available to a broader audience through online repositories and other resources. This can make scientific equipment and instrumentation more accessible to students, hobbyists, and citizen scientists and encourage more people to engage in scientific research and discovery.

Overall, open hardware can improve the quality, reproducibility, and accessibility of scientific research while promoting collaboration, innovation, and creativity among researchers and engineers.

## Join the Open Hardware Movement

We live in a time when innovation and creativity are more crucial than ever. We need to develop new solutions to the challenges we face as a society, from climate change to healthcare to education. And we believe that open hardware can help us achieve these goals.

Open hardware is a way of designing, building and sharing technology accessible to everyone. It is about creating tools and devices that can be modified, improved, and shared freely. By making hardware open, we can enable collaboration, innovation, and creativity on a global scale.
We are calling on you to join us in this movement. Whether you are a scientist working on a new experiment, an engineer building a new machine, or a maker creating something new, we want you to consider open hardware as a way to share your work with the world.

Here are some ways you can get involved:

1. **Start by sharing your work**: If you have a project that you think could benefit from being open, start by sharing it with others. Post your design files, schematics, and code on a platform like GitHub or Thingiverse so that others can build on your work.
2. **Collaborate with others**: Open hardware is all about collaboration. Reach out to other makers and engineers working on similar projects and see if you can work together. By sharing knowledge and resources, we can all benefit.
3. **Use open hardware in your research**: If you are a scientist, consider using open hardware in your research. Using open-source tools and devices ensures that your work is reproducible and accessible to others.
4. **Advocate for open hardware**: Help spread the word about open hardware. Talk to your colleagues and friends about the benefits of open hardware and encourage them to get involved. Attend conferences and events focusing on open hardware and share your experiences with others.

Together, we can create a world where innovation and creativity are accessible to everyone. So let's embrace open hardware and build a better future for all.

## Who are we?

The Open Hardware Community Wageningen is under the umbrella of the OSC-W. If you have any Open Hardware questions, suggestions, or projects you want to highlight, you can contact **Vittorio Saggiomo** (<vittorio.saggiomo@wur.nl> ; Twitter [@V_Saggiomo](https://twitter.com/V_Saggiomo)) Assist—Prof. in the BioNanoTechnology group. If You want to be kept updated, subscribe to the [OSC-W mailing list](https://openscience-wageningen.com/getintouch/).

## Example of Open Hardware at Wageningen

**(Vittorio Saggiomo @ [BioNanoTechnology](https://www.wur.nl/en/research-results/chair-groups/agrotechnology-and-food-sciences/biomolecular-sciences/bionanotechnology-2.htm))**

[Programmable Open Syringe Pumps](https://www.sciencedirect.com/science/article/pii/S2468067221000481) - [GitHub](https://github.com/Vsaggiomo/Ender3-syringe-pumps)

[Automatic Histology / Glass slide preparation](https://www.sciencedirect.com/science/article/pii/S2468067222001158) - [Github](https://github.com/Ponz91/HistoEnder)

Battery Operated / Wifi / <10€ Portable M[]()icroscope - [GitHub](https://matchboxscope.github.io/docs/Matchboxscope)

**([Johannes Hohlbein](https://jhohlbein.com) @ Biophysics)**

[Single Molecule Fluorescence Microscope](https://www.nature.com/articles/s41467-019-11514-0) - [GitHub](https://hohlbeinlab.github.io/miCube/index.html)

__________

*If you have any OH project you want to highlight, please contact Vittorio Saggiomo*
__________

## Open Hardware in other Dutch Universities

[Delft Open Hardware Community](https://www.tudelft.nl/open-hardware)

[Lili's Protolab @ Utrecht University](https://lilis-protolab.sites.uu.nl)

[Innovation Space @ TU/e](https://www.tue.nl/studeren/tue-innovation-space)
