---
title: "Interview with Vittorio Saggiomo"
author: "Ignacio Saldivia"
date: "2022-04-01"
---

<html>
<head>
  <style>
 hr {
  border: 0;
  border-top: 2px solid grey;
 }
  </style>
</head>

<body>

As part of the series on **Open Science Stories**, Wageningen University & Research's library interviewed Vittorio Saggiomo, assistant professor at the BioNanoTechnology group and founder of the [Open Hardware Wageningen movement](https://openscience-wageningen.com/hardwareoscw/).

<img src= "/./images/profilepicsmembers/Vit.jpg" width="200"  style="float:left" hspace="10"/>

### What is open hardware in the world of science?

**Vittorio:** Open hardware follows design principles from the open technology movement, emphasizing transparency, collaboration, and accessibility. For open hardware in science, the design and specifications you use for your experiments are publicly available. Other researchers can build, customize and use the instruments for their experiments. So, not only the research findings and data are published, but also the blueprints, designs, drawings, specifications etc. for the tools are shared.

### What sparked your interest in open hardware?

**Vittorio:** My main drive is that open hardware makes science (experiments) repeatable and the results reproducible. In my tenure track, I started to develop chemical sensors and devices to detect micro pollution in food and soil. The thing that annoyed me most was that I found publications that I could not repeat because the details of the used hardware were not shared. With open hardware, I can repeat experiments with exactly the same instruments, which makes the science more reliable.

**Read the whole interview at the <a href="https://www.wur.nl/en/library/about-the-library/news/show/the-open-science-story-of-vittorio-saggiomo.htm" target='blank'>website of the Wageningen University & Research's library </a>**
