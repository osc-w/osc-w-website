---
title: "Interview with Annika Tensi and Yann de Mey"
author: "Annika Tensi"
date: '2024-04-01'
url: /blog/interview1
---

*This blog is the first of a series in which the [WUR Open Science programme](https://weblog.wur.eu/openscience/launch-open-science-community-wageningen-event-on-13-may-2022/) and the Open Science Community-Wageningen (OSC-W) join forces. The collaboration gives a stage to Wageningen researchers, staff and students who actively support making science more open. We‘ll interview these researchers, staff and students about their motivations and actions for Open Science and also about their hurdles to practice Open Science.*

<img src= "/./images/profilepicsmembers/Annika.jpg" width="150" height="200" style="float:left" hspace="10"/>
<img src= "/./images/profilepicsmembers/Yann.jpg" width="210" height="200" style="float:left" hspace="10"/>

*We’ll start this series with Annika Tensi and Yann de Mey. Annika is the ‘founding mother’ of the OSC-W and Yann is an active community member. Annika is a PhD candidate and Yann is an associate professor. They both work in the Business Economics Group.*

## Question: What is your motivation for doing Open Science?

**Yann:** It started out of frustration to be honest. As a master student, I was working for the AfricaRice, a large CGIAR center. My colleagues there wrote a lot of interesting publications, but I didn’t have access to them. I was really surprised that even the publications within our own institute could not be read by direct colleagues. Open repositories should be the norm, and I am happy that they now are.

When I continued doing research, I noticed that many interesting methods were being applied and many cool datasets were being used as well, which took a lot of time to collect. But none of them were available! So, I started to realize that for me, as a starting researcher, it was very time-consuming to reinvent the wheel again and again. It sometimes even felt like a waste of time to replicate what others had done. It also occurred to me that I didn’t have the proper tools to test the methods that I was trying to develop, as I didn’t have the original data. So,  the process  was really inefficient, and I needed to invest my time in replicating where it would have been better to spend my time innovating. Then, I wondered why open datasets and methods are not the norm…

> <font color=#005073> *When I advanced in my career, I noticed the tension and arguments for making work open or not. However, the reasons for not making a project openly available are usually very old-fashioned. Many authors prefer to protect things for themselves, whereas the core idea of science is to make the world better and to build upon each other’s work: innovations are faster in Open Science!* </font>

**Annika:** The main reasons are the replication crisis, p-hacking and harking, and other fraudulent practices. What we find with these fraudulent practices is actually not the truth – and it is already very difficult to find ‘the truth’: are our models actually describing real life? For me, the abolishment of these fraudulent practices is, in the end, a way to improve science and to get closer to ‘the truth’. Open Science is an important means for this process.

## Question: What is your experience with Open Science?

**Annika:** I was introduced to Open Science principles by one of my co-authors. That was in fact the first time I’d heard about Open Science, and it really opened my eyes. That was the first time I preregistered my studies and my studies after that. As I am in the very beginning of my research career, I haven’t published Open Access yet, but I am planning to do this for my publications, and I also want to publish my codes and datasets if possible.

I think it’s also very important to open code in an understandable way. It’s frustrating that, when I find an interesting analysis, I can’t find the code for it or that a thorough explanation is missing, so that I cannot replicate the analyses with my own data.

**Yann:** Yes, I now make most of my datasets, code and publications openly available. However, it’s also evolved over time, where in the past this was up to you and now it is increasingly being enforced by funders, and also supported by institutions. So, I have put Open Science into practice, but with varying degrees of openness, also depending on to what extent I’ve been supported in this process.

> <font color=#005073> *I am a firm believer that we should be doing more in the future, and that Open Science should be embedded in every aspect of our work – not only in our science, but also in our education.* </font>

## Question: What are your reasons to launch the OSC-W / become an active member?

**Yann:** It’s important to mention the tensions that exist. On the one hand, there is a top-down decision that Open Science needs to be done, and then there is the bottom-up approach that people want to do this. I learn so much from these people, for example, for teaching. I receive so many nice examples from Twitter and open science platforms. This openness is amazing. But then there’s also the tension from above that we need to do this and I don’t feel 100% supported.

Finding all the available information and tools can be quite challenging. That’s why I strongly believe in an Open Science Community Wageningen because I think that we can help each other with such grassroot actions and communication. With both a top-down (institution, funders) and bottom-up (community) approach, we can close the existing Open Science gaps.

**Annika:** My main idea for setting up an Open Science Community was to raise  awareness that if we engage in open science, we all win – both as individual researchers and as an institution. Thus, science wins as a profession.

**Yann:** To complement this, I’d like to state that the bottom-up community approach has a lot of passion, enthusiasm and energy. If we share this passion, we may inspire others. This is something we need to do, not just from the outside, but from the inside – so that others see what can be done and what the power of Open Science is. In this way it’ll also be much easier to convince others to put Open Science into practice.

**Annika:** As we have a huge network of Open Science Communities in the Netherlands and abroad, we can also extend this enthusiasm outside Wageningen.

## Question: Are there any hurdles you need to take regarding Open Science?

**Yann:** It’s a long journey and a continuous process to make Open Science the norm. Researchers need to regard Open Science as an integral part of their routine work to really make a change. For example, I’m in touch with a research group in Zurich led by Prof. Robert Finger that actually “does” Open Science every afternoon on the last Friday of each month. Then, they sit together and discuss what they’ve produced that month: methods, data, publications, blogpost etcetera. Then they make sure that it’s all available online and explained with blogposts if needed. So, they collectively reserve half a day to do this, which is not much, but it’s an amazing example of a bottom-up Open Science approach.

**Annika:** To add to this continuous process and to the awareness, I think it’s important that Open Science is part of the scientific workflow. The perceived hurdle for many researchers is that Open Science adds to their workload. So, Open Science should become incorporated into the workflow. In this way, the additional workload will be much lower. For instance, if we start to write code in an understandable and reproducible way right from the beginning, it takes just one click to share the code. You also benefit yourself because your future self will also be able to understand your current self’s code.

Funding can be another issue. If you upload your code, data or other materials to a platform and you need to pay for it, it might not be funded after your project period has ended. This needs to be taken into account.

Another hurdle can be the imposters syndrome, especially for PhD students or other researchers at the beginning of their career. By this I mean that if you openly show what you have done, you also make yourself vulnerable because everybody can see your mistakes. So, in the Open Science era, the research culture also needs to change.

It should become OK to make mistakes, as we are all in the middle of a big learning process. It should be regarded as an achievement to open up materials because in this way you can improve your code or your process using the feedback from others.

**Yann adds:** This is a very important point. We need to have many examples from researchers in various career stages to show this is all very normal. It also happens to full professors, and they learn from it. Researchers should be open about this and inform others about how their open work has been improved through the feedback of others.

## Question: What are the future plans of the OSC-W?

**Annika:** The community offers low key peer-to-peer advice on how to get your research more open, and also an easy entry point to Open Science. We hope to organise various events on Open Science tools and practices. You can easily join these events. It doesn’t matter whether you’re a novice or an expert, there is no hurdle.

**Yann:** The core component is that it’s a community: you learn from peers. It’s not a set of documents or guidelines or rules, but a group of people who are eager to learn from each other and share experiences. The fact that you learn from other people who are doing the same things is very powerful.

## Question: What has Open Science brought you?

**Yann:** It’s brought me a lot. Whenever I look for materials for courses, I crowdsource a lot of ideas, pictures etc. through networks, blogs and Twitter. I learned a lot for my courses. With regard to research it’s not all open yet, but I see it’s changing. It’s becoming much more common to share materials.

**Annika:** As a PhD candidate I don’t have lots of research experience yet, but I gave a talk on preregistration at the Open Science Community Utrecht. There, I came into contact with a researcher who was doing replication research and we’ve started to collaborate. Together with numerous other European researchers, we’ve replicated a paper in the field of behavioural economics. Furthermore, I have a rather large research network that I gained through the Open Science Community itself.

**Interviewer: Chantal Hukkelhoven**
