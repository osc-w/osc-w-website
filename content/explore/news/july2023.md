---
title: "July 2023"
author: "Marijke van den Berg"
date: '2023-07-15'
---

Dear Open Science Community,  

Welcome to the July edition of the OSC-W newsletter! In this last newsletter before the Summer holidays, you can read about upcoming events, such as the launch symposium of the Dutch Reproducibility Network, a contribution of the Peer Community In to the Open Science Festival in Rotterdam, and more Open Science-related news. We wish you a lovely restful Summer! We can look back on a productive and interesting academic year with many seminars and are looking forward to continuing that after the break.  

### Upcoming events

We’re on a Summer break, so there won’t be any events organised by us for a while. But in the meantime you can look forward to events organised by others, as presented two headers down.  

### Any suggestions?  

If you happen to have any suggestions for events that you would like to be organized or if you maybe have ideas for topics that you’d like to get more information on, feel free to let us know at <osc-wageningen@wur.nl>.

### OS-related news  

#### 31-08-2023: National Open Science Festival Rotterdam

Get ready for the third edition of the Netherlands National Open Science Festival! After last year’s successful event with over 300 visitors, this year the festival will be held at the Erasmus University in Rotterdam. Like the previous editions, the festival revolves around sharing knowledge and peer learning: it is aimed at active researchers from universities (of applied sciences), academic hospitals and research institutes in the Netherlands, and policymakers who enable Open Science practices to spread throughout the academic world. In several workshops and sessions, you will exchange experiences and learn about the current state of affairs regarding Open Science. These will all be in-person, as only the plenary sessions will be livestreamed. For more information, see opensciencefestival.nl.  

#### 31-08-2023: "Peer Community In Agriculture" at the National OS Festival Rotterdam  

At 14.00, David Katzin, a researcher at WUR, will introduce the concept of Peer Community (PCI), a unique non-profit and open peer review and publication system, to the general audience. Building a peer community specifically for the agricultural sciences is particularly relevant for OSC-W and WUR.

#### 02-10-2023: SENSE course: Embracing openness: An introduction to Open Science practices (1.0 ECTS)  

This course is composed of five modules based on various teaching activities, including lectures, podcasts, guided discussions/debates, and exercises. PhD candidates and other early career researchers will finish the course with an Open Science plan for their own work. Register here.

#### 25-10-2023:  DelftX Open Science MOOC  

The enrolment for the DelftX Open Science MOOC is now open. In this introductory course, you will learn the objectives, main concepts, and benefits of Open Science principles along with practices for Open Data management and Open Data sharing.

#### 25-10-2023: Dutch Reproducibility Network – launch symposium, University of Amsterdam

The one-day event will bring together stakeholders from different parts of the research ecosystem — including peer-led grassroots initiatives, research institutions, research support services, and external stakeholders — to build connections, identify needs and resources, showcase existing efforts, and start shaping the reproducibility agenda. Registration details and a detailed program will follow. For the latest information, visit our website: <http://reproducibilitynetwork.nl>.  

Kind regards,  

Marijke for OSC-W  
