---
title: "February 2023"
author: "Annika Tensi"
date: '2023-02-15'
---

Dear Open Science Community,  

Welcome to the February edition of the OSC-W newsletter! In this newsletter you can read about upcoming events, the European Open Science cloud, the NWO Open Science fund, and a free online workshop on how to do reproducible research with R packages.

### Upcoming events

#### Lunch seminar on Citizen Science by Rosan van Halsema  

March 9, 12:00-13:00 h., main building NIOO-KNAW (Colloquium room)  

Rosan van Halsema (NIOO-KNAW, Aquatic Ecology) will speak about Citizen Science at NIOO, and especially about her own experiences with it in her research on water quality in smaller water bodies. For more information and the registration link, see our website.

### Coming up

For the following months, you can expect regular lunch seminars in April and June, but an extra special big event in May. More details will follow soon!

### Any suggestions?  

If you happen to have any suggestions for events that you would like to be organized or if you maybe have ideas for topics that you’d like to get more information on, feel free to let us know at <osc-wageningen@wur.nl>.

### OS-related news  

Free online workshop Reproducible Research with R Packages, offered by the eScience Center Digital Skills Programme. This workshop will help you make your research reproducible, by taking your R script and turning it into a shareable package. It is aimed at PhD candidates and other researchers or software developers in the Netherlands and consist of four weekly sessions between March 8 and March 29. Find out more here.

#### NWO Open Science Fund 2023: first application deadline 09-05-2023

The NWO Open Science Fund grants (max. €50,000,-) are meant to support projects that implement and support open-science practices. They particularly encourage projects that: “improve how good open science practice is recognized and rewarded; transform the way researchers publish; develop or adapt interoperability standards; develop, test or adapt open platforms or tools for wider community use; stimulate wider adoption of open science practice among researchers; further the adoption of citizen science approaches”.

#### European Open Science Cloud (EOSC)

The EOSC has been in development since 2015 already, but now the final product is coming closer. While its name suggests that the EOSC is meant to store data, it’s actually meant to function as a gigantic data-sharing system, where researchers still host their own data, while it is findable for others. The EOSC Association is planning to have a smaller version, with a more limited amount of data, up and running this year.

Kind regards,

Marijke for OSC-W  
