---
title: "November 2022"
author: "Annika Tensi"
date: '2022-11-15'
---


Dear Open Science Community,

It is always nice to get responses and feedback from you! We read the replies and feedback to the October Newsletter with great pleasure and we were especially happy to welcome two new core group members after the call. Our goal is to become a real community, in which activities and topics are suggested by you, while ‘the core group’ is just the facilitator. So, if you have a burning topic that you want to share with the community, discuss your personal OS problem, have an event idea or want to contribute in any creative way possible, let us know.

The first week of November is already in the books – and this edition of the OSC-W Newsletter is a little bit delayed - but, we still want to share upcoming events and updates from the world of open science at WUR. Here you go!

### Lunch Seminars

The slides of the last lunch seminar by David Katzin are published on our website. If you cannot make it to the seminar, make sure to check it out. The slides contain some very helpful resources for open source coding!

In the next lunch seminar on 23. November (12.30-13.30hrs, Forum/online) two INRAE scientists, Thomas Guillemaud and Denis Bourguet, will discuss what Peer Community In (PCI) and the associated Peer Community Journal are, how they work, how they contribute to the publication system and how researchers can best make use of them. Take a look at our website for further information. Please sign up as soon as possible!
The last lunch seminar of the year will be on a replication study in the social sciences. Does this sound interesting to you? Further information will follow. Keep a close eye on our Twitter and website.

### Pre-registration webinar

A while ago, Harm Veling gave a workshop on pre-registration. If you want to get more information on the ins and outs of pre-registering your study, you have now the chance to look back at the seminar here. And if you want to read on, check out the new Open Science blog post on pre-registrations.

### MOOC Transformative Citizen Science for Sustainability

During the launch event of the Citizen Science Hub, the Citizen Science for Sustainability MOOC was launched. Are you interested to learn how to design projects that actively mobilise citizens, policymakers and scientists? Then, join the online course on principles and practices for Transformative Citizen Science for Sustainability. Further information here.

Last, but not least, we would like to ask you to help us spread the word about OSC-W: Please forward the newsletter to your colleagues. Maybe they want to subscribe to the newsletter, too?!

### Open Science related news, initiatives, courses and events in and outside WUR

Policy: NPOS2030 Ambition Document
In the coming decade, the ministry will invest heavily in Open Science (20 million/year). Funding will be in line with the National Open Science Programme (NPOS) multi-annual plan, which will be drafted by the end of this year based on the NPOS Rolling Agenda. Some time ago, we, all members of Dutch OSCs were invited to provide feedback on the first version of the NPOS rolling agenda. A summary of that feedback is provided here: <https://osf.io/vpgrb>

---

Course: Open Access Publishing
Within the context of the WUR Library’s essentials course series, the course ‘Open Access Publishing’ is taking place on Monday 12 December 12.30-13.15 hrs. More information and registration via [this link](https://www.wur.nl/en/library/researchers/courses-and-demos-staff/courses-and-demos-display/wur-library-essentials-open-access-publishing.htm)

---

Course: Research Data Management
Within the context of the WUR Library’s essentials course series, the course ‘Research Data Management’ is taking place on Monday 14 November 13.00-13.45 hrs. More information and registration via [this link](https://www.wur.nl/en/library/researchers/courses-and-demos-staff/courses-and-demos-display/wur-library-essentials-research-data-management.htm).

---

News: [NWO Open Approach to Open Science](https://www.nwo.nl/en/news/open-approach-open-science?utm_campaign=nwo_nieuwsbrief&utm_medium=newsletter&utm_source=mailing&utm_content=2022-20)

---

Petition: Open letter to the WHOSTP and Subcommittee on Open Science
<https://ostp-letter.github.io/>

Cheers,

Annika Tensi for OSC-W
