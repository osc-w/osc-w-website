---
title: "December 2022"
author: "Annika Tensi"
date: '2022-12-15'
---

Dear Open Science Community,

Isn’t it crazy how fast this year is coming to an end again? And what a year it has been – the official launch of the OSC-W took place in May, we had monthly lunch seminars and now, as the grand finale of 2022, we survey all NIOO-KNAW and WUR employees.

### 📣 Open Science Monitor 📣

Never heard about Open Science before? Or are you THE Open Science expert? We would love to hear your opinion either way. We are conducting a 10-minute survey because we want to know more about the Open Science landscape at our institutions.

Maybe you even want to support us in getting a good picture of our Open Science landscape: We invite you to send the link to three of your colleagues. Survey results will be publicly available.

### Lunch Seminars

The last lunch seminar, hosted and organized by Daniel Tamarit, was on preprints, open peer review and Peer Community In. In case you missed it, the slides are available on our website. If you want to watch the recording, please send us an e-mail.

In the last lunch seminar of the year, Julia Höhler will present the results and experience of a social science replication project. The seminar takes place on 14. December (12.30-13.30hrs, Lebo/online). Further information and sign-up on the website.

We are going to kick-start the new year with a lunch seminar initiated and hosted by Daniel Reyes Lastiri on 11. January (12.30-13.30hrs, Impulse). The seminar will be on how to maintain and combine existing models. Further details and sign-up here. The seminar will be interactive, involving different stakeholders. It will take place in the Speakers’ corner in Impulse on campus. If you are interested in being a panellist, please send us an e-mail.

Last, but not least, we would like to ask you to help us spread the word about OSC-W: Please forward the newsletter to your colleagues. Maybe they want to subscribe to the newsletter, too?! We are also always looking for people interested in joining our core group and taking up some more responsibilities. Just send us an e-mail.

Cheers,

Annika Tensi for OSC-W
