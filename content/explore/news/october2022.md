---
title: "October 2022"
author: "Annika Tensi"
date: '2022-10-15'
---

Dear Open Science Community,

How much of the research, done at your department, would you describe as ‘open’ (aka accessible, reproducible, transparent, and inclusive)? And could you guess how many WUR researchers are actively using Open Science practices and tools? Well, to be honest, at the moment, we can’t answer these questions, so we need your help!

In this October edition of the OSC-W newsletter, you find out how you can help us and we give you a glimpse into our upcoming projects and events.

*!!! Check out the new section at the bottom of the Newsletter: Open Science-related news, initiatives, courses and events in and outside WUR !!!*

### Open Science Monitor @WUR – Share your opinion

To gain insights into the Open Science attitudes and beliefs among employees from WUR and NIOO-KNAW, we are going to conduct an Open Science Monitor. The Monitor will be a concise online survey. We aim to investigate how much is known about Open Science and whether you experience any obstacles to putting Open Science into practice. Yet, we face the challenge of how to reach especially the colleagues who are less familiar with Open Science and let their very important opinions be heard. Do you have an idea of how to get in contact with them? Any experts for this in the OSC-W? Please help!

### Lunch Seminars

The last lunch seminar was a great success! Thanks to the very engaging speaker, Yizhou Ma, for sharing his interesting research on 3D food manufacturing and opinions on open-source tools. Thanks also to the big and active audience! It was fun to discuss with all of you.

In the next lunch seminar on 27. October (12.30-13.30hrs, Orion/online) David Katzin will discuss factors that facilitate and impede open source coding in greenhouse horticulture: what are potential benefits and drawbacks? What are the attitudes toward this practice? How does open source fit within the general framework of open science? And what steps should we take to make our own code open source? Take a look at our website for further information and to sign up.

### Citizen Science Hub launch event

The OSC-W welcomes a new community under the big umbrella of [WUR’s Open Science & Education Program](https://www.wur.nl/en/about-wur/our-values/integrity/finding-answers-together/open-science-education.htm?_ga=2.133563275.422800864.1664448833-1249442804.1664448833): the Citizen Science Hub! The WUR Citizen Science Hub aims to connect and strengthen the citizen science community in and around WUR. Join their launch event on Thursday, 20. October, 15.00 - 17.00hrs, at the atrium of Plus Ultra 2 (Bronland 10). During this event, two WUR citizen science initiatives will be launched: the Transformative Citizen Science for Sustainability online course and the WUR Citizen Science Hub. This will also be your chance to get an introduction to Wageningen Library’s citizen science initiative, the Stadslab. Further information and sign-up (important!) here.

### OSC-W core group

At the moment, we are a very nice, small and diverse group of Open Science enthusiasts to introduce and foster Open Science at our institution. Every three weeks, we have an informal meeting in which we brainstorm about upcoming events and projects, discuss developments related to OS and share ideas on how to inspire colleagues to embed open science practices and values in their workflows. If this sounds like fun to you – no matter whether you are an OS expert or a rookie – join our core group!

In the near future, we are also looking for a successor for the position of the OSC-W ‘chair’. You would connect to many other Open Science enthusiasts within and outside WUR and be part of a transition in the academic world, with broad cultural and practical implications. Therefore, you keep an overview of the OSC-W’s activities and take the initiative for meetings etc. Together with the fellow OSC-W core group, you organise events and coordinate projects and communication (e.g. this Newsletter, the website, and our Twitter channel). You will receive help and there is also budget available for activities. If you want to know more: shoot us a message!

### Open Science-related news, initiatives, courses and events in and outside WUR

News:

[The quota for open-access publishing with Springer will be reached in early November.](https://www.wur.nl/en/library/about-the-library/news/show/2022-quota-for-open-access-publishing-with-springer-will-be-reached-early-november.htm)

---

Intranet Group: [Wageningen Modelling Group](https://intranet.wur.nl/Project/WRModellingToolbox)

The Wageningen Modelling Group (WMG) is set up to organize support for the development, maintenance and integrated application of computer models at WUR

---

Course: Towards FAIR Data Management

26 October, 3 and 10 November 2022

Why should data be shared in a FAIR way? What stands behind this concept on the technology side? How does FAIR data look like, how to use, and how to create it? Which relevant platforms and tools exist and how to use them? This course will address these questions by providing relevant information and possibilities to get skills to work with and implement FAIR data in practice.

Read more: <https://www.pe-rc.nl/node/22722>

---

Workshop: Wageningen Model & Datasteward Workshop
24 November 2022

The Wageningen Modelling Group joins forces with the Data Stewards Network to bring you a workshop exploring a wide range of modelling and data management-related topics. Join us in introducing, discussing and working on topics that matter to you and your peers.
In the program plenary presentations by Wouter Hendriks (WUR-Dean of Science) on the importance of model and data management, Marta Teperek (TU Delft) on collaboration in research and Tia Hermans (WUR - to be confirmed) on the role of model collaboration within the Integrated Regional Approach (or 'stikstof aanpak'). And plenty of opportunity for hands-on data & software carpentry, discussing models, and meeting up with colleagues within the Wageningen modelling community.

[Program and sign-up](https://www.wur.nl/en/value-creation-cooperation/collaborating-with-wur-1/wdcc/show-wdcc/wageningen-model-datasteward-workshop.htm)

---

Course: [Research Data Management](https://www.wur.nl/en/article/research-data-management.htm)

November/December 2022
This course for PhD candidates and postdocs is offered by Wageningen Graduate Schools and organised by WUR Library. It consists of lectures and practical assignments that cover various aspects of managing research data: from organising your data files during data collection to publishing your final data set.

---

Cheers,

Annika Tensi for OSC-W
