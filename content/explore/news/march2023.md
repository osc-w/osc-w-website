---
title: "March 2023"
author: "Marijke"
date: '2023-03-15'
---

Dear Open Science Community,  

Welcome to the March edition of the OSC-W newsletter! In this newsletter you can read about the upcoming events, our progress with the results of the survey, and more external OS-related news.

### Upcoming events

#### 06-04-2023: Lunch seminar by Barbara Terlouw (Plant sciences)

From Twitter post to community-driven annotation marathon: The creation of MIBiG 3.0.  

In this talk, Barbara Terlouw will discuss how they mobilised and coordinated 86 researchers from four different continents to combine their efforts into this mammoth annotation effort, and how we ensured consistent annotation quality throughout the process. For more information and a sign up link, see our website.

#### 08-06-2023: Lunch seminar by Merle Schots (Citizen Science Hub)  

Merle Schots will speak about Citizen Science, more information will follow.  

### Survey results

Some time ago, we sent out a survey to WUR and NIOO to gain insight into the Open Science landscape at the institutions. The results are currently being analysed and compared to the results of Utrecht University. We’ll be able to show you the definitive results soon.

### Any suggestions?  

If you happen to have any suggestions for events that you would like to be organized or if you maybe have ideas for topics that you’d like to get more information on, feel free to let us know at <osc-wageningen@wur.nl>.

### OS-related news  

#### NWO Open Science Fund 2023: first application deadline 09-05-2023

The NWO Open Science Fund grants (max. €50,000,-) are meant to support projects that implement and support open-science practices. They particularly encourage projects that: “improve how good open science practice is recognized and rewarded; transform the way researchers publish; develop or adapt interoperability standards; develop, test or adapt open platforms or tools for wider community use; stimulate wider adoption of open science practice among researchers; further the adoption of citizen science approaches”.

#### 02-05-2023: Open Science event KU Leuven

Do you want to know more about Open Science and exchange experiences with fellow researchers? Join us at the annual KU Leuven Open Science day on 2 May. This year, we have a rich programme, with presentations and posters from scholars with diverse backgrounds; early career researchers and professors, from humanities, biomedical sciences and science, engineering and technology. Our keynote speakers are Muki Haklay (UCL London) and Thomas Margoni (KU Leuven’s Centre for IT & IP Law). For more information, see their website.

#### 31-08-2023: National Open Science Festival Rotterdam

Get ready for the third edition of the Netherlands National Open Science Festival! After last year’s successful event with over 300 visitors, this year the festival will be held at the Erasmus University in Rotterdam. Like the previous editions, the festival revolves around sharing knowledge and peer learning: it is aimed at active researchers from universities (of applied sciences), academic hospitals and research institutes in the Netherlands, and policymakers who enable Open Science practices to spread throughout the academic world. In several workshops and sessions you will exchange experiences and learn about the current state of affairs regarding Open Science. These will all be in-person, as only the plenary sessions will be livestreamed. For more information, see opensciencefestival.nl.  

Kind regards,  

Marijke for OSC-W  
