---
title: "January 2023"
author: "Annika Tensi"
date: '2023-01-15'
---

Dear Open Science Community,

Welcome to the first OSC-W newsletter of 2023! We wish you all the best and we hope that this new year will be filled with lots of Open Science practices.  

### Thank you, Annika

Until the end of last year, Annika Tensi was the chair – and much more – of OSC-W. We’re all grateful for her hard work and dedication to the Open Science Community and wish her all the best in the rest of her career!  

This means that the position of chair is now vacant. Most of the tasks have been divided among the core group members for the time being.

### Upcoming events

#### Breakfast seminar on Frugal Science by Benedict Diederich  

February 2, 09:30-10:30 h., in Impulse (speaker’s corner)

Benedict Diederich (co-founder of open-hardware startup openUC2) will speak about Frugal Science, which focuses on the use and development of scientific instruments – especially microscopes – with limited budget and resources, to answer societally relevant questions. For more information and the registration link, see our website.  

#### Lunch seminar on FAIR by Design by Jasper Koehorst

February 14, 12:30-13:30 h., in Impulse (speaker’s corner)

Jasper Koehorst (WUR, Laboratory of Systems and Synthetic Biology) will speak about the way he applies the FAIR by Design principles in his own work on microbial communities. For more information and the registration link, see our website.  

#### Lunch seminar on Citizen Science by Rosan van Halsema

March 9, 12:00-13:00 h., main building NIOO-KNAW (Colloquium room)

Rosan van Halsema (NIOO-KNAW, Aquatic Ecology) will speak about Citizen Science at NIOO, and especially about her own experiences with it in her research on water quality in smaller water bodies. For more information and the registration link, see our website.  

### Open Science Monitor: 10-minute survey

If you have not yet done so, please remember to participate in our survey on Open Science. And if you already have completed it, it’d be great if you could forward it to some of your colleagues. It doesn’t matter if you’re an Open Science expert or if you’ve never heard of it, or anything in between, we’re curious to hear your input to identify any obstacles and opportunities to practice Open Science at both WUR and NIOO-KNAW. You can find the survey here.  

### Any suggestions?

If you happen to have any suggestions for events that you would like to be organized or if you maybe have ideas for topics that you’d like to get more information on, feel free to let us know at <osc-wageningen@wur.nl>.  

### OS-related news

Free online workshop Reproducible Research with R Packages, offered by the eScience Center Digital Skills Programme. This workshop will help you make your research reproducible, by taking your R script and turning it into a shareable package. It is aimed at PhD candidates and other researchers or software developers in the Netherlands and consists of four weekly sessions between March 8 and March 29. Find out more here.  

Kind regards,

Marijke for OSC-W
