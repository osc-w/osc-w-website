---
title: "Submission information"
author: "Annika Tensi"
date: '2022-03-15'
---

### Submit your own Open Science case

The submission form requires the following information, if you apply for the Lighthouse Award yourself:

| Information | Description |
| ----------- | ----------- |
| **Contact information** | |
| **Case Study Title** | The case study should be no more than 800 words in total. |
| **Open Science objectives/practices** | Please specify one or more of the Open Science objectives/practices addressed in your case study |
| **Introduction** | Please provide a brief description of the open practice(s) and tool(s) used, as well as the context in which the open practices were used. |
| **Motivation** | Why did you use the Open Science practice(s) (e.g., what are the benefits and for whom) |
| **Lessons learned** | Please reflect on the barriers or challenges and/or supporting factors (e.g., supervisor, workshops, infrastructure, funding) encountered.  |

Additional information:

- How much extra time did the open practices require?
- URLs, references and further information
- word count (excluding title, URLs, references and further information)

### Nominate a colleague

The submission form requires the following information, if you nominate a colleague:

| Information | Description |
| ----------- | ----------- |
| **Your contact information** | Your nomination is anonymous |
| **The contact information of the nominee** | After your nomination, we will contact the nominee to ask for permission.  |
| **Motivation** | Please describe why you think that your colleague deserves the first Open Science Lighthouse Award. |

The description should be no more than 500 words in total. You can include information on the Open Science objectives/practices that your colleague advances, how they inspire you and others to open your research or how your colleague and others are benefiting from their open research practices.
