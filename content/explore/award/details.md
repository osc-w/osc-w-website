---
title: "Details Submission Award"
author: "Annika Tensi"
date: '2022-03-15'
---

### Procedure

Submitted entries will be screened for eligibility by members of OSC-W. The jury consists of members of the OSC-W core group. The organizers choose the best entries from the eligible submissions. The applicants will be notified by the end of April 2022 and invited to present their case studies as success stories during the OSC-W launch event in May 2022. The eligible submissions will be showcased on the OSC-W website.

### Objectives

Applicants should describe activities that align with one or more of the following Open Science objectives:

- Making the outputs of research, including publications, data, software and other research materials freely accessible while considering the FAIR data sharing principles.
- Using online tools and services to increase the transparency of research processes and methodologies.
- Making scientific research more reproducible by increasing the amount and quality of information placed on the public record.
- Using alternative models of publication and peer review to make the dissemination and certification of research faster and more transparent.
- Using open collaborative methods and tools to increase efficiency and widen participation in research.

### Practices

The case study should describe one or more of the Open Science practices listed below:

- Using publication under an open license to communicate research outputs, which may include publications, data, software code, and web resources
 NB: given that open access publishing is nowadays the most common publication mode for scholarly articles, having published open access articles alone will not be enough to qualify an entry as eligible
- Disseminating research findings as a preprint, either independently of formal submission to a journal, or as part of a journal’s open peer review procedure
- Providing an open peer review of a paper submitted under a formal peer review process managed by a publisher
- Creating a public pre-registration of a study design or publishing a study as a registered report
- Incorporating open and participatory methods into the design and conduct of research, e.g., by using open notebook-based methods or creating a project using a ‘citizen science’ online platform
- Introducing Open Science concepts and practices into teaching and learning
- Creating new tools or technologies to facilitate Open Science practices, e.g., for combining or repurposing datasets and other research outputs from separate locations or disciplines, or for mining content
- Undertaking activities to develop the environment for Open Science, e.g., by engaging in high-profile communications, by causing a journal to adopt pro-Open Science policies, or by participating in community initiatives to develop data or metadata standards

### Case Study examples

These are some examples of suitable subjects for a case study:

- A dataset or software source code created by you has been made openly available and then re-used by researchers or other end-users, e.g., to inform policymaking or develop services or products
- You are a humanities researcher who has created an open web resource and consider the practicalities and challenges of sustaining long-term access and usability
- You have recently submitted an article through a publisher’s open peer review system, and discuss your experience and some of the pros and cons of open peer review
- You conduct qualitative social science research exploring sensitive issues and discuss the ethical and practical challenges of sharing data collected from participants
- You have developed a new software tool to facilitate Open Science, e.g., to combine or repurpose datasets from disparate sources
- You have participated in a community group to develop data or metadata standards and tools for your discipline
- In your teaching you have introduced students to Open Science principles and practices, for example by setting replication study assignments.
