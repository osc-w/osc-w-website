---
title: Lighthouse Award
author: "Annika Tensi"
date: '2022-06-03'

---

<html>
<head>
<style>
img {
  float: right;
}
.underlink a:link {
  color: #005172;
  text-decoration: underline;
}
</style>
</head>

<body>

<p style="font-size: 20px"><img style=" margin: 0px 0px 0px 15px;" alt="Picture of the Lighthouse award" src= "/./images/awardLogos/award.png" width="30%" height="40">
With the Open Science Lighthouse Award, we want to celebrate and reward WUR researchers and researchers from associated institutes (e.g., NIOO-KNAW) who have used open practices and tools to make their research more transparent, accessible and reproducible. <br> <br>
We hope that the Open Science success stories of WUR researchers encourage more colleagues to open their research and promote the bottom-up adoption of open science practices across the science groups and research institutes.
<br> <br>  Whether it is engaging with open research workflows and pre-registering experiments, sharing data and code, being involved in citizen science and communicating with stakeholders, or adopting other exemplary open science practices, with the Lighthouse Award, we prise researchers who make open science part of their daily practice. We invite researchers to consider their full research life cycles and outputs in all possible forms, including papers, data, software and hardware, methods and infrastructure. <br>  <br> You can find further information on the submissions 2022 below. </p>
</body>
</html>
