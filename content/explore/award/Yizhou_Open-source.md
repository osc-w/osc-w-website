---
title: 'Yizhou Ma Submission'
author: "Annika Tensi"
date: '2022-06-02'
---

# Yizhou Ma

## Open-source software development to accelerate adoption of 3D food printing

### Open Science Objectives & Practices

My projects aim to develop open-source software packages and publish open-assess journal articles to accelerate the adoption of 3D food printing as an emerging food processing technique. Specifically, I publish datasets with the FAIR data principles, host open-source software codes on the WUR GitLab repository, and record software demos on YouTube.

### Introduction

In my PhD thesis research, I develop a library of software packages to monitor and control 3D printing of foods.

I have published 1 open-access journal article with the dataset and analysis codes publicly available through the WUR's GitLab repository. The repository comes with a README file that allows others to reproduce my study results, reuse the scripts for new data, and perform secondary analysis on my dataset.

I have another under-review manuscript with the intent to publish with open assess. Together with the manuscript, we host another GitLab repository for an open-source software. The software is developed under the Creative Commons license and has been used by another collaboration project between WUR and Technical University of Eindhoven. I also recorded a demo video to help others use the software.

In the pipeline, I have another open-source software that has been tested internally in the Food Process Engineering group. This software is currently hosted on an internal GitLab repository with the intent to be open-source once the development is finished.

### Motivation

For me, open science is not a choice but the norm. During my studies, I have been benefited very much from open-source software and open science. It is fair to say that I "grew up" with open science. I'm fully immersed with the open science practices, so it is only logical for me to do the same and contribute the same to the scientific community.

To address the question, my study results are only meaningful if others are using them. To best help others to use my outcomes, I prepare everything based on the open-science practices. Because the 3D food printing community is still at its infancy, other developers and equipment manufacturers can directly adapt my software and solutions to their productions, accelerating the technological adoption of 3D food printing.

The concept of my project also extends to other food manufacturing practices, so making my methods and data open-source will help future researchers to implement similar solutions. Seeing others using my work and learn from my mistakes is fulfilling to me.

### Lessons learned

It is important to get permissions and supports from supervisors and other project stakeholders before implementing the open-science practices. We should have a clear list of outcomes on top of the traditional practice (normally just one manuscript). The list of outcomes can be annotated software scripts, tutorial videos, labelled and well-noted datasets, etc. We need to set the expectations beforehand and get all hands on deck.

Open science indeed comes with a price in terms of time and resources. We had technical difficulties for properly setting up GitLab through the WUR network infrastructure. Now we have a troubleshoot document to continuously improve our workflow with open-source programming.

I took the data management course from Wageningen Data Competency Center, and it was a great overview of available resources to achieve open science at WUR. I recommend researchers to take that course.

### How much extra time did the open practices require?

About 20% overhead on top of the regular time required

### URLs, references and further information

[Open access publication](https://www.sciencedirect.com/science/article/pii/S146685642100165X)

Open-source GitLab repositories:
[1](https://git.wur.nl/yizhou.ma/food-extrudability-assessment-and-prediction)
[2](https://git.wur.nl/yizhou.ma/extrusion-flow-tracker)
