---
title: "Lighthouse Award 2022"
author: "Annika Tensi"
date: '2022-03-16'

url: /award/Lighthouse_Award_2022

---

# Win the first Open Science Lighthouse Award
|<div style="width:250px"></div>  | <div style="width:150px"></div>  | 
| -----------| ----------- |
| With the Open Science Lighthouse Award, we want to celebrate and reward WUR researchers and researchers from associated institutes (e.g., NIOO-KNAW) who have used open practices and tools to make their research more transparent, accessible and reproducible.  <br> <br> We hope that the Open Science success stories of WUR researchers encourage more colleagues to open their research and promote the bottom-up adoption of open science practices across the science groups and research institutes. |![Lighthouse with text: Win the first Open Science Lighthouse Award](/images/lighthouse_txt.jpg) |

## Submission is closed
You can either submit your own Open Science case story to apply for the Lighthouse Award or you can nominate a colleague. There are two separate submission forms. Submission is open for students, researchers and support staff on all levels. 

### **Closing date for submission: 25 April 2022**

## Present your case at the OSC-W launch event
The researcher with the most compelling, eye-opening or provocative Open Science story receives the first Open Science Lighthouse Award! All eligible submissions have the chance to present their case at the festive Open Science Community Wageningen (OSC-W) launch event in May (10 min presentation). All eligible case studies will be given public attention through the OSC-W Website.

## What are we looking for
Whether it is engaging with open research workflows and pre-registering experiments, sharing data and code, being involved in citizen science and communicating with stakeholders, or adopting other exemplary open science practices, we are looking for researchers who make open science part of their daily practice. We invite researchers to consider their full research life cycles and outputs in all possible forms, including papers, data, software and hardware, methods and infrastructure.  

