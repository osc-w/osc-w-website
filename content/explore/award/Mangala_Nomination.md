---
title: 'Special Nomination: Mangala Srinivas'
author: "Annika Tensi"
output: html_document
date: '2022-06-03'

url: /award/Mangala_Nomination
---
*by Christine Jansen*

### Please describe why you think your colleague deserves the first Open Science Lighthouse Award
Open Science (OS) really came into the spotlight in Europe with the implementation of Plan S. While there is no doubt about the need and benefits of OS, it takes time to switch from the traditional rewarding in science which is strongly embedded in current tenure tracks to rewards based on sharing and openness. Mangala Srinivas has been working for several years, to promote OS, and to ensure that the necessary changes to the assessment criteria for researchers occur (Recognition and Rewards, R&R). Mangala was able to really push this agenda through her role as Chair of the Young Academy of Europe (YAE). Mangala was part of a  keynote panel discussion at the release of the new R&R strategy by the VSNU and EUA, at the launch of their white paper “Room for everyone’s talent” in 2019 (1). The key point she made was that researchers cannot be assessed by current standards (particularly journal impact factors) while also being required to make their research open as this 
puts them in an untenable position. In the same year, Mangala released a statement, together with EuroDoc and the Marie Curie Alumni Association to this effect (2,3). She also delivered a keynote address at a joint event on Plan S, organised by the Academia Europaea and KU Leuven (4), on “The Future of Research”. In 2020, Mangala participated in the Regional Consultation on Open Science for Western Europe and North America by UNESCO (5), again encouraging changes in the R&R to allow for more OS.  After joining WUR in May 2021 as Chair of CBI (Cellbiology & Immunology group), Mangala has published 2 further articles in Nature on the urgent need for change (2021, Why science needs a 
new R&R system(6)), and the need for adapting the current R&R schemes to encourage diversity (together with the Wageningen Young Academy, in press). 

The data for her 2021 Nature article was published in Open Research Europe (ORE) as a Data Note (7) to allow broad access to the (anonymised) data. 

Mangala is on the Scientific Advisory Board  of ORE (8), and was recently interviewed for an article commemorating the 1-year anniversary of ORE in the EC magazine, Horizon(9). She stressed that although platforms like the ORE are essential, they will not be widely accepted until they are indexed in academic search engines like PubMed or Google Scholar.

Mangala stimulated the implementation of a comprehensive Data Management Plan (DMP) at CBI, together with other colleagues. The DMP emphasizes FAIR data and accountability, while promoting working with collaborators, both private and public. This has a direct impact on the way data is structured and managed within CBI. Translating a DMP to practical use and action on the ground is a challenge that she, and others at CBI, are now tackling.

Overall, Mangala has been working to promote OS for years now, while working to ensure that it does not disadvantage vulnerable researchers. I would like to recommend her for this award.


### URLs, references and further information
[1.](https://www.youtube.com/watch?v=YTXMt8tffQg)
[2.](http://eurodoc.net/implementation-plan-s.pdf)
[3.](http://www.eurodoc.net/news/2019/euinvestinknowledge-researchers-calls-on-national-governments-ineurope-to-protect-eu)
[4.](https://yacadeuro.org/tag/plan-s/)
[5.](https://en.unesco.org/science-sustainable-future/open-science/regional-consultations/westerneuropean-north-america)
[6.](https://www-nature-com.ezproxy.library.wur.nl/articles/d41586-021-01952-6)
[7.](https://open-research-europe.ec.europa.eu/articles/1-138)
[8.](https://think.f1000research.com/open-research-europe-submission/scientific-advisory-board/?utm_source=CPB&utm_medium=cms&utm_campaign=JQC19228#)
[9.](https://ec.europa.eu/research-and-innovation/en/horizon-magazine/open-science-publishing-strikes-richseam-ore-publishing-platform)
